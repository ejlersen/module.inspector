﻿using UnityEditor;

namespace Module.Inspector.Editor
{
    public abstract class HandlePropertyDrawer : AbstractPropertyDrawer
    {
        public abstract void Draw(HandleDrawerPropertyAttribute attribute, SerializedProperty serializedProperty);
        public abstract bool IsValidFor(SerializedProperty serializedProperty);
    }
}