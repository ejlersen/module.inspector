﻿using Module.Inspector.Editor.Utilities;
using UnityEditor;
using UnityEngine;

namespace Module.Inspector.Editor
{
    public abstract class DrawerPropertyDrawer : AbstractPropertyDrawer
    {
        public abstract bool Draw(Rect position, DrawerPropertyAttribute attribute, SerializedProperty property, GUIContent label, EditorPropertyUtility.Result result);
        public abstract string GetErrorMessage(SerializedProperty property);
    }
}