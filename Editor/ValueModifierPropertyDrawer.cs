﻿using UnityEditor;

namespace Module.Inspector.Editor
{
    public abstract class ValueModifierPropertyDrawer : AbstractPropertyDrawer
    {
        public abstract void Modify(ValueModifierPropertyAttribute attribute, SerializedProperty property);
    }
}