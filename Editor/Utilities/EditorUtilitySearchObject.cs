﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Module.Inspector.Editor
{
    internal static class EditorUtilitySearchObject
    {
        private static readonly Dictionary<Type, SearchObject> TYPE_TO_ASSETS = new();
        
        public static SearchObject SearchAndCache(Type type)
        {
            if (TYPE_TO_ASSETS.TryGetValue(type, out SearchObject searchObject))
                return searchObject;
            
            string[] guids = AssetDatabase.FindAssets($"t:{type.Name}");
            searchObject = new();

            for (var i = 0; i < guids.Length; i++)
            {
                string path = AssetDatabase.GUIDToAssetPath(guids[i]);
                Object asset = AssetDatabase.LoadAssetAtPath(path, type);
                
                if (asset != null)
                    searchObject.assets.Add(asset);
            }
            
            searchObject.CreateNamesFromAssets();
            TYPE_TO_ASSETS.Add(type, searchObject);
            return searchObject;
        }

        public static void Clear()
        {
            TYPE_TO_ASSETS.Clear();
        }
        
        public sealed class SearchObject
        {
            public readonly List<Object> assets = new();
            public GUIContent[] names;
            
            public void CreateNamesFromAssets()
            {
                assets.Sort((a0, a1) => string.Compare(a0.name, a1.name, StringComparison.Ordinal));
                
                names = new GUIContent[assets.Count + 1];
                names[0] = new GUIContent("None");

                for (var i = 0; i < assets.Count; i++)
                {
                    names[i + 1] = new GUIContent(assets[i].name);
                }
            }
        }
    }
}