﻿using System;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Module.Inspector.Editor.Utilities
{
    internal static class EditorSerializedPropertyUtility
    {
        public static bool IsValue(Object obj, string fieldName, object value, bool useFieldValue)
        {
            if (obj == null || string.IsNullOrEmpty(fieldName))
                return false;

            var so = new SerializedObject(obj);
            SerializedProperty sp = so.FindProperty(fieldName);
            return IsValue(sp, value, useFieldValue);
        }
        
        public static bool IsValue(SerializedProperty sp, object value, bool useFieldValue)
        {
            if (sp == null)
                return false;

            if (useFieldValue)
            {
                switch (sp.propertyType)
                {
                    case SerializedPropertyType.Generic:
                        return value == (object)sp.objectReferenceValue;
                    case SerializedPropertyType.Integer:
                        return value is int i && sp.intValue == i;
                    case SerializedPropertyType.Boolean:
                        return value is bool b && sp.boolValue == b;
                    case SerializedPropertyType.Float:
                        return value is float f && Mathf.Approximately(sp.floatValue, f);
                    case SerializedPropertyType.String:
                        return value is string s && sp.stringValue == s;
                    case SerializedPropertyType.LayerMask:
                        return value is LayerMask l && sp.intValue == l;
                    case SerializedPropertyType.Enum:
                        return value is Enum && sp.intValue == Convert.ToInt32(value);
                }
            }
            else
            {
                if (sp.propertyType == SerializedPropertyType.Boolean)
                    return sp.boolValue;
                if (sp.propertyType == SerializedPropertyType.ObjectReference)
                    return sp.objectReferenceValue != null;
            }
            
            return true;
        }
    }
}