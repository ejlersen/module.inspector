﻿using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Module.Inspector.Editor
{
    internal static class EditorTypeUtility
    {
        private const BindingFlags FIELD_FLAGS = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic;
        
        private static readonly Dictionary<Type, Type[]> DICT_AS_TYPES = new Dictionary<Type, Type[]>();
        private static readonly Dictionary<Type, string[]> DICT_AS_STRS = new Dictionary<Type, string[]>();
        private static readonly Dictionary<Type, string[]> DICT_AS_DESCS = new Dictionary<Type, string[]>();
        private static readonly Dictionary<Type, GUIContent[]> DICT_AS_GUI = new Dictionary<Type, GUIContent[]>();
        private static readonly Dictionary<Type, GUIContent[]> DICT_AS_GUI_SHORTNAME = new Dictionary<Type, GUIContent[]>();
        private static readonly Dictionary<Type, GUIContent[]> DICT_AS_GUI_DESC = new Dictionary<Type, GUIContent[]>();

        private static readonly Dictionary<Type, FieldInfo[]> DICT_AS_FIELDS = new Dictionary<Type, FieldInfo[]>();
        private static readonly Dictionary<Type, string[]> DICT_FIELDS_AS_STRS = new Dictionary<Type, string[]>();
        private static readonly Dictionary<Type, string[]> DICT_FIELDS_AS_DESCS = new Dictionary<Type, string[]>();

        public static Type GetType(string fullname)
        {
            Assembly[] assemblies = AppDomain.CurrentDomain.GetAssemblies();

            for (var i = 0; i < assemblies.Length; i++)
            {
                Assembly assembly = assemblies[i];
                Type type = assembly.GetType(fullname);

                if (type != null)
                    return type;
            }

            return null;
        }

        public static FieldInfo GetField(object target, string fieldName)
        {
            return GetFieldByName(target.GetType(), fieldName);
        }

        public static bool SetFieldValue(object target, string fieldName, object value, bool allowDuplicate)
        {
            if (InternalTrySetUnityObject(target, fieldName, value, allowDuplicate))
                return true;

            FieldInfo field = GetField(target, fieldName);

            if (field == null)
                return false;

            try
            {
                field.SetValue(target, value);
                
                if (target is Object o)
                    EditorUtility.SetDirty(o);
            }
            catch (Exception e)
            {
                Debug.LogException(e);
                return false;
            }
            
            return true;
        }

        private static bool InternalTrySetUnityObject(object target, string fieldName, object value, bool allowDuplicate)
        {
            if (!(target is Object o))
                return false;

            var so = new SerializedObject(o);
            SerializedProperty sp = so.FindProperty(fieldName);
            bool success = sp?.TrySetValueTo(value, allowDuplicate) ?? false;

            if (success)
                so.ApplyModifiedProperties();

            return success;
        }

        internal static Type[] GetAssignableFrom(Type type)
        {
            if (!DICT_AS_TYPES.ContainsKey(type))
                InternalFetch(type);
            
            return DICT_AS_TYPES[type];
        }
        
        internal static string[] GetAssignableFromAsStrings(Type type)
        {
            if (!DICT_AS_STRS.ContainsKey(type))
                InternalFetch(type);
            
            return DICT_AS_STRS[type];
        }

        internal static GUIContent[] GetAssignableFromAsGUI(Type type, bool useFullname = true)
        {
            if (!DICT_AS_GUI.ContainsKey(type))
                InternalFetch(type);
            
            return useFullname ? DICT_AS_GUI[type] : DICT_AS_GUI_SHORTNAME[type];
        }
        
        internal static string[] GetAssignableFromAsDescriptions(Type type)
        {
            if (!DICT_AS_DESCS.ContainsKey(type))
                InternalFetch(type);
            
            return DICT_AS_DESCS[type];
        }
        
        internal static GUIContent[] GetAssignableFromAsGUIDescriptions(Type type)
        {
            if (!DICT_AS_GUI_DESC.ContainsKey(type))
                InternalFetch(type);
            
            return DICT_AS_GUI_DESC[type];
        }
        
        internal static FieldInfo[] GetFields(Type type)
        {
            if (!DICT_AS_FIELDS.ContainsKey(type))
                InternalFetchFields(type);
            
            return DICT_AS_FIELDS[type];
        }

        internal static FieldInfo GetFieldByName(Type type, string fieldName)
        {
            FieldInfo[] fields = GetFields(type);

            for (var i = 0; i < fields.Length; i++)
            {
                if (fields[i].Name.Equals(fieldName))
                    return fields[i];
            }

            return null;
        }

        internal static string[] GetFieldsAsStrings(Type type)
        {
            if (!DICT_FIELDS_AS_STRS.ContainsKey(type))
                InternalFetchFields(type);
            
            return DICT_FIELDS_AS_STRS[type];
        }

        internal static string[] GetFieldsAsDescriptions(Type type)
        {
            if (!DICT_FIELDS_AS_DESCS.ContainsKey(type))
                InternalFetchFields(type);
            
            return DICT_FIELDS_AS_DESCS[type];
        }

        private static void InternalFetch(Type assignableFrom)
        {
            Assembly[] assemblies = AppDomain.CurrentDomain.GetAssemblies();
            var listTypes = new List<Type>(32);

            for (var i = 0; i < assemblies.Length; i++)
            {
                Assembly assembly = assemblies[i];
                Type[] types = assembly.GetTypes();

                for (var j = 0; j < types.Length; j++)
                {
                    Type type = types[j];
                    
                    if (!type.IsAbstract && assignableFrom.IsAssignableFrom(type))
                        listTypes.Add(type);
                }
            }
            
            listTypes.Sort((t0, t1) => string.Compare(t0.Name, t1.Name, StringComparison.Ordinal));
            var fullnames = new string[listTypes.Count];
            var descs = new string[listTypes.Count];
            var guiFullNames = new GUIContent[listTypes.Count];
            var guiShortnames = new GUIContent[listTypes.Count];
            var guiDescs = new GUIContent[listTypes.Count];

            for (var i = 0; i < fullnames.Length; i++)
            {
                fullnames[i] = listTypes[i].FullName;
                descs[i] = $"{listTypes[i].Name} (<i>{fullnames[i]}</i>)";
                guiFullNames[i] = new GUIContent(fullnames[i]);
                guiShortnames[i] = new GUIContent(listTypes[i].Name);
                guiDescs[i] = new GUIContent(descs[i]);
            }
            
            DICT_AS_TYPES.Add(assignableFrom, listTypes.ToArray());
            DICT_AS_STRS.Add(assignableFrom, fullnames);
            DICT_AS_DESCS.Add(assignableFrom, descs);
            DICT_AS_GUI.Add(assignableFrom, guiFullNames);
            DICT_AS_GUI_SHORTNAME.Add(assignableFrom, guiShortnames);
            DICT_AS_GUI_DESC.Add(assignableFrom, guiDescs);
        }
        
        private static void InternalFetchFields(Type type)
        {
            FieldInfo[] fields = type.GetFields(FIELD_FLAGS);
            Array.Sort(fields, (f0, f1) => string.Compare(f0.Name, f1.Name, StringComparison.Ordinal));
            
            var names = new string[fields.Length];
            var descs = new string[fields.Length];

            for (var i = 0; i < names.Length; i++)
            {
                names[i] = fields[i].Name;
                descs[i] = $"{fields[i].Name} (<i>{fields[i].FieldType.Name}</i>)";
            }
            
            DICT_AS_FIELDS.Add(type, fields);
            DICT_FIELDS_AS_STRS.Add(type, names);
            DICT_FIELDS_AS_DESCS.Add(type, descs);
        }
    }
}