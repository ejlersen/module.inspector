﻿using UnityEditor;
using UnityEngine;

namespace Module.Inspector.Editor.Utilities
{
    internal static class EditorIcons
    {
        private static GUIContent CONTENT_WARNING;
        private static GUIContent CONTENT_ERROR;
        private static GUIContent CONTENT_OBJECT;

        private static GUIContent GetWarningIconContent()
        {
            if (CONTENT_WARNING == null)
                CONTENT_WARNING = EditorGUIUtility.IconContent("Warning");
            
            return CONTENT_WARNING;
        }

        private static GUIContent GetErrorIconContent()
        {
            if (CONTENT_ERROR == null)
                CONTENT_ERROR = EditorGUIUtility.IconContent("Error");
            
            return CONTENT_ERROR;
        }
        
        public static GUIContent GetScriptableObjectIconContent()
        {
            if (CONTENT_ERROR == null)
                CONTENT_ERROR = EditorGUIUtility.IconContent("d_ScriptableObject Icon");
            
            return CONTENT_ERROR;
        }

        public static void SetWarningIcon(GUIContent content)
        {
            GUIContent errorContent = GetWarningIconContent();
            content.image = errorContent.image;
        }
        
        public static void SetErrorIcon(GUIContent content)
        {
            GUIContent errorContent = GetErrorIconContent();
            content.image = errorContent.image;
        }
    }
}