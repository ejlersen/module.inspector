﻿using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;

namespace Module.Inspector.Editor.Utilities
{
    public static class EditorNamingUtility
    {
        public static string ConvertTo(NamingAttribute.EPatternType type, string str)
        {
            switch (type)
            {
                case NamingAttribute.EPatternType.CamelCasing:
                    return ToCamel(str);
                case NamingAttribute.EPatternType.PascalCasing:
                    return ToPascal(str);
                case NamingAttribute.EPatternType.SnakeCasing:
                    return ToSnake(str);
                case NamingAttribute.EPatternType.SnakeCasingAllCaps:
                    return ToSnake(str).ToUpper();
                case NamingAttribute.EPatternType.KebabCasing:
                    return ToKebab(str);
                case NamingAttribute.EPatternType.KebabCasingAllCaps:
                    return ToKebab(str).ToUpper();
                default:
                    return str;
            }
        }
        
        public static string ToPascal(string str)
        {
            var pattern = new Regex(@"[A-Z]{2,}(?=[A-Z][a-z]+[0-9]*|\b)|[A-Z]?[a-z]+[0-9]*|[A-Z]|[0-9]+");
            MatchCollection matches = pattern.Matches(str);

            if (matches.Count != 0)
                str = matches.Cast<Match>().Select(c => c.Value).Aggregate(( a, b ) => a + " " + b);
            
            var culture = new CultureInfo("en-US", false);
            str = culture.TextInfo.ToTitleCase(str);
            str = str.Replace(@" ", "");
            return str;
        }

        public static string ToCamel(string str)
        {
            str = ToPascal(str);
            
            if (str.Length > 0)
                str = char.ToLower(str[0]) + str.Substring(1);
            
            return str;
        }

        public static string ToSnake(string str) 
        {
            var pattern = new Regex(@"[A-Z]{2,}(?=[A-Z][a-z]+[0-9]*|\b)|[A-Z]?[a-z]+[0-9]*|[A-Z]|[0-9]+");
            MatchCollection matches = pattern.Matches(str);

            if (matches.Count != 0)
                str = matches.Cast<Match>().Select(c => c.Value).Aggregate(( a, b ) => a + "_" + b);

            return str;
        }

        public static string ToKebab(string str) 
        {
            var pattern = new Regex(@"[A-Z]{2,}(?=[A-Z][a-z]+[0-9]*|\b)|[A-Z]?[a-z]+[0-9]*|[A-Z]|[0-9]+");
            MatchCollection matches = pattern.Matches(str);

            if (matches.Count != 0)
                str = matches.Cast<Match>().Select(c => c.Value).Aggregate(( a, b ) => a + "-" + b);

            return str;
        }
    }
}