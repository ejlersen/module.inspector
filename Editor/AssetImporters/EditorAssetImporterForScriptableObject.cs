﻿using UnityEditor;

namespace Module.Inspector.Editor.AssetImporters
{
    internal sealed class EditorAssetImporterForScriptableObject : AssetPostprocessor
    {
        public static void OnPostprocessAllAssets(string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths)
        {
            if (HasAnyScriptableObjects(importedAssets) || 
                HasAnyScriptableObjects(deletedAssets) || 
                HasAnyScriptableObjects(movedAssets))
            {
                EditorUtilitySearchObject.Clear();
            }
        }

        private static bool HasAnyScriptableObjects(string[] paths)
        {
            for (var i = 0; i < paths.Length; i++)
            {
                string path = paths[i];

                if (path.EndsWith(".asset"))
                    return true;
            }

            return false;
        }
    }
}