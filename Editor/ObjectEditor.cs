﻿using Module.Inspector.Editor.Utilities;
using UnityEditor;
using UnityEngine;

namespace Module.Inspector.Editor
{
    [CanEditMultipleObjects]
    [CustomEditor(typeof(Object), true, isFallback = true)]
    public sealed class ObjectEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            if (Event.current.type == EventType.Repaint)
                HandleDrawerEditor.Clear();
            
            DrawDefaultInspector();
            DrawHiddenInspector();
            DrawMethodInspector();
        }
        
        private void DrawHiddenInspector()
        {
            if (!IsValidTarget())
                return;
            
            EditorHiddenFieldUtility.EditableFieldInfo[] fields = EditorHiddenFieldUtility.Query(target);

            if (fields.Length == 0)
                return;
            
            float totalHeight = EditorHiddenFieldUtility.CalculateHeight(fields) + EditorGUIUtility.singleLineHeight * 2;
            GUILayout.Space(totalHeight);

            Rect position = GUILayoutUtility.GetLastRect();
            position.y -= totalHeight;

            #if UNITY_2020_1_OR_NEWER
            position.width = EditorGUIUtility.currentViewWidth - 22.0f;
            #endif

            position.y = position.yMax + EditorGUIUtility.singleLineHeight;
            position.height = EditorGUIUtility.singleLineHeight;
            EditorGUI.LabelField(position, "Hidden fields", EditorStyles.miniBoldLabel);

            position.y += EditorGUIUtility.singleLineHeight;

            for (var i = 0; i < fields.Length; i++)
            {
                EditorHiddenFieldUtility.Draw(position, fields[i], target);
                position.y += position.height;
            }
        }

        private void DrawMethodInspector()
        {
            if (!IsValidTarget())
                return;
            
            EditorMethodUtility.ResultPrimary[] primaries = EditorMethodUtility.QueryPrimary(target);

            if (primaries == null || primaries.Length == 0)
                return;
            
            float totalHeight = EditorMethodUtility.CalculateHeight(target, primaries) + EditorGUIUtility.singleLineHeight * 2;
            GUILayout.Space(totalHeight);
            
            var position = new Rect(0, 0, 1, 1);
            
            if (Event.current.type == EventType.Repaint)
            {
                position = GUILayoutUtility.GetLastRect();
                position.y -= totalHeight;

                #if UNITY_2020_1_OR_NEWER
                position.width = EditorGUIUtility.currentViewWidth - 22.0f;
                #endif
            }

            position.y = position.yMax + EditorGUIUtility.singleLineHeight;
            position.height = EditorGUIUtility.singleLineHeight;
            EditorGUI.LabelField(position, "Buttons", EditorStyles.miniBoldLabel);

            position.y += EditorGUIUtility.singleLineHeight;

            for (var i = 0; i < primaries.Length; i++)
            {
                EditorMethodUtility.ResultPrimary primary = primaries[i];

                float height = primary.drawer.GetHeight(target, primary.methodInfo);
                position.height = height;

                primary.drawer.OnGUI(position, target, primary.methodInfo);
                position.y += height;
            }
        }

        private bool IsValidTarget()
        {
            return target != null && targets.Length == 1;
        }
    }
}