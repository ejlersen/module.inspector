﻿namespace Module.Inspector.Editor
{
    public enum EAccessType : byte
    {
        Enabled,
        Hidden,
        ReadOnly,
    }
}