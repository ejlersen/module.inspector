﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Module.Inspector.Editor
{
    [InitializeOnLoad]
    internal static class HandleDrawerEditor
    {
        private static readonly List<Element> ELEMENTS = new List<Element>();

        static HandleDrawerEditor()
        {
            SceneView.duringSceneGui -= SceneGUI;
            SceneView.duringSceneGui += SceneGUI;

            Selection.selectionChanged -= OnSelectionChanged;
            Selection.selectionChanged += OnSelectionChanged;
        }

        private static void SceneGUI(SceneView view)
        {
            for (int i = ELEMENTS.Count - 1; i >= 0; i--)
            {
                Element e = ELEMENTS[i];
                bool valid = e.target != null;

                if (e.target != null)
                {
                    var serializedObject = new SerializedObject(e.target);
                    SerializedProperty serializedProperty = serializedObject.FindProperty(e.propertyPath);
                    valid = serializedProperty != null && e.drawer.IsValidFor(serializedProperty);
                    
                    if (valid)
                    {
                        EditorGUI.BeginChangeCheck();
                        e.drawer.Draw(e.attribute, serializedProperty);
                        
                        if (EditorGUI.EndChangeCheck())
                            serializedObject.ApplyModifiedProperties();
                    }
                    
                    serializedObject.Dispose();
                }
                
                if (!valid)
                    ELEMENTS.RemoveAt(i);
            }
        }

        private static void OnSelectionChanged()
        {
            ELEMENTS.Clear();
        }

        public static void Add(HandleDrawerPropertyAttribute attribute, SerializedProperty property, HandlePropertyDrawer drawer)
        {
            ELEMENTS.Add(new Element(attribute, property, drawer));
        }

        public static void Clear()
        {
            ELEMENTS.Clear();
        }

        private sealed class Element
        {
            public readonly HandleDrawerPropertyAttribute attribute;
            public readonly HandlePropertyDrawer drawer;
            public readonly Object target;
            public readonly string propertyPath;

            public Element(HandleDrawerPropertyAttribute attribute, SerializedProperty property, HandlePropertyDrawer drawer)
            {
                this.attribute = attribute;
                this.drawer = drawer;
                
                target = property.serializedObject.targetObject;
                propertyPath = property.propertyPath;
            }
        }
    }
}