﻿using System.Text;
using Module.Inspector.Editor.Utilities;
using UnityEditor;
using UnityEngine;

namespace Module.Inspector.Editor
{
    [CustomPropertyDrawer(typeof(LabelHandleAttribute))]
    internal sealed class LabelHandleAttributeDrawer : HandlePropertyDrawer
    {
        private readonly StringBuilder builder = new StringBuilder();
        private GUIStyle style;
        
        public override void Draw(HandleDrawerPropertyAttribute attribute, SerializedProperty serializedProperty)
        {
            var att = (LabelHandleAttribute)attribute;
            Vector3 wp = EditorHandleUtility.GetWorldPosition(serializedProperty, att.fieldPosition, att.space);

            if (style == null)
            {
                style = new GUIStyle(GUI.skin.label);
                style.alignment = TextAnchor.MiddleCenter;
            }
            
            builder.Clear();

            if ((att.type & ELabelType.Field) != 0)
                builder.Append(serializedProperty.displayName);

            if ((att.type & ELabelType.Value) != 0)
            {
                if ((att.type & ELabelType.Field) != 0)
                    builder.Append('\n');

                builder.Append(serializedProperty.GetValueAsString());
            }
            
            Handles.Label(wp, builder.ToString(), style);
        }
        
        public override bool IsValidFor(SerializedProperty serializedProperty)
        {
            return true;
        }
    }
}