﻿using Module.Inspector.Editor.Utilities;
using UnityEditor;
using UnityEngine;

namespace Module.Inspector.Editor
{
    [CustomPropertyDrawer(typeof(RectangleHandleAttribute))]
    internal sealed class RectangleHandleAttributeDrawer : HandlePropertyDrawer
    {
        public override void Draw(HandleDrawerPropertyAttribute attribute, SerializedProperty serializedProperty)
        {
            var att = (RectangleHandleAttribute)attribute;
            bool hasPositionField = EditorHandleUtility.TryGetWorldPosition(serializedProperty, att.fieldPosition, att.space, out Vector3 worldPosition);
            bool hasRotationField = EditorHandleUtility.TryGetWorldRotation(serializedProperty, att.fieldRotation, att.space, out Quaternion worldRotation);
            Vector2 size = GetValue(serializedProperty, out EAxis axii);
            Color color = Handles.color;

            if (hasPositionField)
                worldPosition = EditorHandleUtility.PositionHandle(worldPosition, worldRotation);
            if (hasRotationField)
                worldRotation = EditorHandleUtility.RotationHandle(worldPosition, worldRotation);

            size = EditorHandleUtility.RectHandle(worldPosition, worldRotation, size, axii);
            
            if (hasPositionField)
                EditorHandleUtility.SetWorldPosition(serializedProperty, att.fieldPosition, worldPosition, att.space);
            if (hasRotationField)
                EditorHandleUtility.SetWorldRotation(serializedProperty, att.fieldRotation, worldRotation, att.space);
            
            SetValue(serializedProperty, size);
            Handles.color = color;
        }

        private Vector2 GetValue(SerializedProperty serializedProperty, out EAxis axii)
        {
            switch (serializedProperty.propertyType)
            {
                case SerializedPropertyType.Integer:
                    axii = EAxis.X;
                    return new Vector2(serializedProperty.intValue, serializedProperty.intValue);
                case SerializedPropertyType.Float:
                    axii = EAxis.X;
                    return new Vector2(serializedProperty.floatValue, serializedProperty.floatValue);
                case SerializedPropertyType.Vector2:
                    axii = EAxis.All;
                    return serializedProperty.vector2Value;
                case SerializedPropertyType.Vector3:
                    axii = EAxis.All;
                    return serializedProperty.vector3Value;
                case SerializedPropertyType.Vector2Int:
                    axii = EAxis.All;
                    return serializedProperty.vector2IntValue;
                case SerializedPropertyType.Vector3Int:
                    axii = EAxis.All;
                    return new Vector2(serializedProperty.vector3IntValue.x, serializedProperty.vector3IntValue.y);
                default:
                    axii = EAxis.None;
                    return Vector2.one;
            }
        }
        
        private void SetValue(SerializedProperty serializedProperty, Vector2 value)
        {
            switch (serializedProperty.propertyType)
            {
                case SerializedPropertyType.Integer:
                    serializedProperty.intValue = Mathf.RoundToInt(value.x);
                    break;
                case SerializedPropertyType.Float:
                    serializedProperty.floatValue = value.x;
                    break;
                case SerializedPropertyType.Vector2:
                    serializedProperty.vector2Value = value;
                    break;
                case SerializedPropertyType.Vector3:
                    serializedProperty.vector3Value = value;
                    break;
                case SerializedPropertyType.Vector2Int:
                    serializedProperty.vector2IntValue = new Vector2Int(Mathf.RoundToInt(value.x), Mathf.RoundToInt(value.y));
                    break;
                case SerializedPropertyType.Vector3Int:
                    serializedProperty.vector3IntValue = new Vector3Int(Mathf.RoundToInt(value.x), Mathf.RoundToInt(value.y), 0);
                    break;
            }
        }

        public override bool IsValidFor(SerializedProperty serializedProperty)
        {
            switch (serializedProperty.propertyType)
            {
                case SerializedPropertyType.Integer:
                case SerializedPropertyType.Float:
                case SerializedPropertyType.Vector2:
                case SerializedPropertyType.Vector3:
                case SerializedPropertyType.Vector2Int:
                case SerializedPropertyType.Vector3Int:
                    return true;
                default:
                    return false;
            }
        }
    }
}