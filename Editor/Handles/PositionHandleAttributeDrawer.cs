﻿using Module.Inspector.Editor.Utilities;
using UnityEditor;
using UnityEngine;

namespace Module.Inspector.Editor
{
    [CustomPropertyDrawer(typeof(PositionHandleAttribute))]
    internal sealed class PositionHandleAttributeDrawer : HandlePropertyDrawer
    {
        public override void Draw(HandleDrawerPropertyAttribute attribute, SerializedProperty serializedProperty)
        {
            var att = (PositionHandleAttribute)attribute;
            Vector3 worldPosition = EditorHandleUtility.GetWorldPosition(serializedProperty, att.space);
            Quaternion worldRotation = EditorHandleUtility.GetWorldRotation(serializedProperty, att.space);
            Color color = Handles.color;

            worldPosition = EditorHandleUtility.PositionHandle(worldPosition, worldRotation, att.axiis);
            EditorHandleUtility.SetWorldPosition(serializedProperty, worldPosition, att.space);
            Handles.color = color;
        }

        public override bool IsValidFor(SerializedProperty serializedProperty)
        {
            switch (serializedProperty.propertyType)
            {
                case SerializedPropertyType.Integer:
                case SerializedPropertyType.Float:
                case SerializedPropertyType.Vector2:
                case SerializedPropertyType.Vector3:
                case SerializedPropertyType.Vector2Int:
                case SerializedPropertyType.Vector3Int:
                    return true;
                case SerializedPropertyType.ObjectReference:
                    return serializedProperty.objectReferenceValue != null && serializedProperty.objectReferenceValue is Transform;
                default:
                    return false;
            }
        }
    }
}