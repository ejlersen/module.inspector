﻿using Module.Inspector.Editor.Utilities;
using UnityEditor;
using UnityEngine;

namespace Module.Inspector.Editor
{
    [CustomPropertyDrawer(typeof(RotationHandleAttribute))]
    internal sealed class RotationHandleAttributeDrawer : HandlePropertyDrawer
    {
        public override void Draw(HandleDrawerPropertyAttribute attribute, SerializedProperty serializedProperty)
        {
            var att = (RotationHandleAttribute)attribute;
            bool hasPositionField = EditorHandleUtility.TryGetWorldPosition(serializedProperty, att.fieldPosition, att.space, out Vector3 worldPosition);
            Quaternion worldRotation = EditorHandleUtility.GetWorldRotation(serializedProperty, att.space);
            Color color = Handles.color;

            if (hasPositionField)
                worldPosition = EditorHandleUtility.PositionHandle(worldPosition, worldRotation);

            worldRotation = EditorHandleUtility.RotationHandle(worldPosition, worldRotation);
            
            if (hasPositionField)
                EditorHandleUtility.SetWorldPosition(serializedProperty, att.fieldPosition, worldPosition, att.space);

            EditorHandleUtility.SetWorldRotation(serializedProperty, worldRotation, att.space);
            Handles.color = color;
        }
        
        public override bool IsValidFor(SerializedProperty serializedProperty)
        {
            switch (serializedProperty.propertyType)
            {
                case SerializedPropertyType.Quaternion:
                    return true;
                default:
                    return false;
            }
        }
    }
}