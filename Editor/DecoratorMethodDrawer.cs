﻿using UnityEngine;

namespace Module.Inspector.Editor
{
    public abstract class DecoratorMethodDrawer : AbstractMethodDrawer
    {
        public abstract bool Draw(Rect position, DecoratorMethodAttribute attribute);
        public abstract float GetHeight(DecoratorMethodAttribute attribute);
    }
}