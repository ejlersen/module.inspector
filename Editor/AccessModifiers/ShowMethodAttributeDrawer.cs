﻿using System.Reflection;
using Module.Inspector.Editor.Utilities;
using UnityEngine;

namespace Module.Inspector.Editor
{
    [CustomMethodDrawer(typeof(ShowMethodAttribute))]
    internal sealed class ShowMethodAttributeDrawer : AccessModifierMethodDrawer
    {
        public override EAccessType GetAccessType(AccessModifierMethodAttribute attribute, Object target, MethodInfo methodInfo, EAccessType currentAccessType)
        {
            if (currentAccessType != EAccessType.Hidden && !GetVisibleValue(attribute, target))
                currentAccessType = EAccessType.Hidden;

            return currentAccessType;
        }
        
        private static bool GetVisibleValue(AccessModifierMethodAttribute attribute, Object target)
        {
            var att = (ShowMethodAttribute)attribute;
            return EditorSerializedPropertyUtility.IsValue(target, att.fieldName, att.fieldValue, att.useFieldValue);
        }
    }
}