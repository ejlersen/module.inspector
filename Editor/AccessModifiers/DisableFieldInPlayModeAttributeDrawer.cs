﻿using UnityEditor;
using UnityEngine;

namespace Module.Inspector.Editor
{
    [CustomPropertyDrawer(typeof(DisableFieldInPlayModeAttribute))]
    internal sealed class DisableFieldInPlayModeAttributeDrawer : AccessModifierPropertyDrawer
    {
        public override EAccessType GetAccessType(AccessModifierPropertyAttribute attribute, SerializedProperty property, EAccessType currentAccessType)
        {
            if (currentAccessType == EAccessType.Enabled && Application.isPlaying)
                currentAccessType = EAccessType.ReadOnly;

            return currentAccessType;
        }
    }
}