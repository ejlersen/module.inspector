﻿using System.Reflection;
using UnityEngine;

namespace Module.Inspector.Editor
{
    [CustomMethodDrawer(typeof(EnableMethodInPlayModeAttribute))]
    internal sealed class EnableMethodInPlayModeAttributeDrawer : AccessModifierMethodDrawer
    {
        public override EAccessType GetAccessType(AccessModifierMethodAttribute attribute, Object target, MethodInfo methodInfo, EAccessType currentAccessType)
        {
            if (currentAccessType == EAccessType.Enabled && !Application.isPlaying)
                currentAccessType = EAccessType.ReadOnly;

            return currentAccessType;
        }
    }
}