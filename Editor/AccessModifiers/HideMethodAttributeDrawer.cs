﻿using System.Reflection;
using Module.Inspector.Editor.Utilities;
using UnityEngine;

namespace Module.Inspector.Editor
{
    [CustomMethodDrawer(typeof(HideMethodAttribute))]
    internal sealed class HideMethodAttributeDrawer : AccessModifierMethodDrawer
    {
        public override EAccessType GetAccessType(AccessModifierMethodAttribute attribute, Object target, MethodInfo methodInfo, EAccessType currentAccessType)
        {
            if (currentAccessType != EAccessType.Hidden && GetHideValue(attribute, target))
                currentAccessType = EAccessType.Hidden;

            return currentAccessType;
        }

        private static bool GetHideValue(AccessModifierMethodAttribute attribute, Object target)
        {
            var att = (HideMethodAttribute)attribute;
            return string.IsNullOrEmpty(att.fieldName) || EditorSerializedPropertyUtility.IsValue(target, att.fieldName, att.fieldValue, att.useFieldValue);
        }
    }
}