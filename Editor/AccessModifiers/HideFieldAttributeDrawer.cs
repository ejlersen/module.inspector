﻿using UnityEditor;

namespace Module.Inspector.Editor
{
    [CustomPropertyDrawer(typeof(HideFieldAttribute))]
    internal sealed class HideFieldAttributeDrawer : AccessModifierPropertyDrawer
    {
        public override EAccessType GetAccessType(AccessModifierPropertyAttribute attribute, SerializedProperty property, EAccessType currentAccessType)
        {
            if (currentAccessType != EAccessType.Hidden && GetHideValue(attribute, property))
                currentAccessType = EAccessType.Hidden;

            return currentAccessType;
        }
        
        private static bool GetHideValue(AccessModifierPropertyAttribute attribute, SerializedProperty sp)
        {
            var att = (HideFieldAttribute)attribute;
            return string.IsNullOrEmpty(att.fieldName) || sp.IsSiblingValue(att.fieldName, att.fieldValue, att.useFieldValue);
        }
    }
}