﻿using System.Reflection;
using UnityEngine;

namespace Module.Inspector.Editor
{
    [CustomMethodDrawer(typeof(HideInNormalInspectorAttribute))]
    internal sealed class HideInNormalInspectorAttributeDrawer : AccessModifierMethodDrawer
    {
        public override EAccessType GetAccessType(AccessModifierMethodAttribute attribute, Object target, MethodInfo methodInfo, EAccessType currentAccessType)
        {
            return EAccessType.Hidden;
        }
    }
}