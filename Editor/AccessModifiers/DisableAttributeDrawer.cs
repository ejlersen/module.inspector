﻿using UnityEditor;

namespace Module.Inspector.Editor
{
    [CustomPropertyDrawer(typeof(DisableAttribute))]
    internal sealed class DisableAttributeDrawer : AccessModifierPropertyDrawer
    {
        public override EAccessType GetAccessType(AccessModifierPropertyAttribute attribute, SerializedProperty property, EAccessType currentAccessType)
        {
            return currentAccessType == EAccessType.Enabled ? EAccessType.ReadOnly : currentAccessType;
        }
    }
}