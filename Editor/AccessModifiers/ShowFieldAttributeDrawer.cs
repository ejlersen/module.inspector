﻿using UnityEditor;

namespace Module.Inspector.Editor
{
    [CustomPropertyDrawer(typeof(ShowFieldAttribute))]
    internal sealed class ShowFieldAttributeDrawer : AccessModifierPropertyDrawer
    {
        public override EAccessType GetAccessType(AccessModifierPropertyAttribute attribute, SerializedProperty property, EAccessType currentAccessType)
        {
            if (currentAccessType != EAccessType.Hidden && !GetVisibleValue(attribute, property))
                currentAccessType = EAccessType.Hidden;

            return currentAccessType;
        }
        
        private static bool GetVisibleValue(AccessModifierPropertyAttribute attribute, SerializedProperty sp)
        {
            var att = (ShowFieldAttribute)attribute;
            return sp.IsSiblingValue(att.fieldName, att.fieldValue, att.useFieldValue);
        }
    }
}