﻿using System.Reflection;
using Module.Inspector.Editor.Utilities;
using UnityEngine;

namespace Module.Inspector.Editor
{
    [CustomMethodDrawer(typeof(EnableMethodAttribute))]
    internal sealed class EnableMethodAttributeDrawer : AccessModifierMethodDrawer
    {
        public override EAccessType GetAccessType(AccessModifierMethodAttribute attribute, Object target, MethodInfo methodInfo, EAccessType currentAccessType)
        {
            var att = (EnableMethodAttribute)attribute;
            
            if (currentAccessType == EAccessType.Enabled && !EditorSerializedPropertyUtility.IsValue(target, att.fieldName, att.fieldValue, att.useFieldValue))
                currentAccessType = EAccessType.ReadOnly;

            return currentAccessType;
        }
    }
}