﻿using UnityEditor;

namespace Module.Inspector.Editor
{
    [CustomPropertyDrawer(typeof(DisableFieldAttribute))]
    internal sealed class DisableFieldAttributeDrawer : AccessModifierPropertyDrawer
    {
        public override EAccessType GetAccessType(AccessModifierPropertyAttribute attribute, SerializedProperty property, EAccessType currentAccessType)
        {
            var att = (DisableFieldAttribute)attribute;
            
            if (currentAccessType == EAccessType.Enabled && property.IsSiblingValue(att.fieldName, att.fieldValue, att.useFieldValue))
                currentAccessType = EAccessType.ReadOnly;

            return currentAccessType;
        }
    }
}