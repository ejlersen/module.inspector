﻿using UnityEditor;

namespace Module.Inspector.Editor
{
    [CustomPropertyDrawer(typeof(EnableFieldAttribute))]
    internal sealed class EnableFieldAttributeDrawer : AccessModifierPropertyDrawer
    {
        public override EAccessType GetAccessType(AccessModifierPropertyAttribute attribute, SerializedProperty property, EAccessType currentAccessType)
        {
            var att = (EnableFieldAttribute)attribute;
            
            if (currentAccessType == EAccessType.Enabled && !property.IsSiblingValue(att.fieldName, att.fieldValue, att.useFieldValue))
                currentAccessType = EAccessType.ReadOnly;

            return currentAccessType;
        }
    }
}