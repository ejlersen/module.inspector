﻿using System.Reflection;
using Module.Inspector.Editor.Utilities;
using UnityEngine;

namespace Module.Inspector.Editor
{
    [CustomMethodDrawer(typeof(DisableMethodAttribute))]
    internal sealed class DisableMethodAttributeDrawer : AccessModifierMethodDrawer
    {
        public override EAccessType GetAccessType(AccessModifierMethodAttribute attribute, Object target, MethodInfo methodInfo, EAccessType currentAccessType)
        {
            var att = (DisableMethodAttribute)attribute;
            
            if (currentAccessType == EAccessType.Enabled && EditorSerializedPropertyUtility.IsValue(target, att.fieldName, att.fieldValue, att.useFieldValue))
                currentAccessType = EAccessType.ReadOnly;

            return currentAccessType;
        }
    }
}