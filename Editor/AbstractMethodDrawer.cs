﻿using System.Reflection;
using Module.Inspector.Editor.Utilities;
using UnityEditor;
using UnityEngine;

namespace Module.Inspector.Editor
{
    public abstract class AbstractMethodDrawer
    {
        public void OnGUI(Rect position, Object target, MethodInfo method)
        {
            EditorMethodUtility.Result result = EditorMethodUtility.Query(method);
            EAccessType accessType = GetAccessType(target, method, result);

            if (accessType == EAccessType.Hidden)
                return;

            if (result.decorator != null)
            {
                var rect = new Rect(position);
                rect.height = result.decorator.drawer.GetHeight(result.decorator.attribute);
                result.decorator.drawer.Draw(rect, result.decorator.attribute);
                
                position.height -= rect.height;
                position.y += rect.height;
            }
            
            bool prevEnabled = GUI.enabled;
            GUI.enabled = accessType == EAccessType.Enabled;

            if (result.draw != null)
            {
                if (!result.draw.drawer.Draw(position, result.draw.attribute, target, method))
                    EditorGUI.LabelField(position, method.Name, result.draw.drawer.GetErrorMessage());
            }

            GUI.enabled = prevEnabled;
        }

        public float GetHeight(Object target, MethodInfo method)
        {
            EditorMethodUtility.Result result = EditorMethodUtility.Query(method);
            EAccessType accessType = GetAccessType(target, method, result);

            if (accessType == EAccessType.Hidden)
                return 0.0f;

            float height = EditorGUIUtility.singleLineHeight;

            if (result.decorator != null)
                height += result.decorator.drawer.GetHeight(result.decorator.attribute);
            
            return height;
        }

        private EAccessType GetAccessType(Object target, MethodInfo method, EditorMethodUtility.Result result)
        {
            if (result.accessModifiers.Count == 0)
                return EAccessType.Enabled;

            EAccessType accessType = 0;
                
            for (var i = 0; i < result.accessModifiers.Count; i++)
            {
                EditorMethodUtility.ResultValue<AccessModifierMethodAttribute, AccessModifierMethodDrawer> value = result.accessModifiers[i];
                accessType = value.drawer.GetAccessType(value.attribute, target, method, accessType);
            }

            return accessType;
        }
    }
}