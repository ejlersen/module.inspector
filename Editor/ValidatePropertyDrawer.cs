﻿using UnityEditor;

namespace Module.Inspector.Editor
{
    public abstract class ValidatePropertyDrawer : AbstractPropertyDrawer
    {
        public abstract bool Validate(ValidatePropertyAttribute attribute, SerializedProperty property);
        public abstract string GetValidationError(ValidatePropertyAttribute attribute, SerializedProperty property);
    }
}