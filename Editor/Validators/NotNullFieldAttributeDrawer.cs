﻿using UnityEditor;

namespace Module.Inspector.Editor
{
    [CustomPropertyDrawer(typeof(NotNullFieldAttribute))]
    internal sealed class NotNullFieldAttributeDrawer : ValidatePropertyDrawer
    {
        public override bool Validate(ValidatePropertyAttribute attribute, SerializedProperty property)
        {
            switch (property.propertyType)
            {
                case SerializedPropertyType.Integer:
                case SerializedPropertyType.Boolean:
                case SerializedPropertyType.Float:
                case SerializedPropertyType.String:
                case SerializedPropertyType.Color:
                case SerializedPropertyType.LayerMask:
                case SerializedPropertyType.Enum:
                case SerializedPropertyType.Vector2:
                case SerializedPropertyType.Vector3:
                case SerializedPropertyType.Vector4:
                case SerializedPropertyType.Rect:
                case SerializedPropertyType.ArraySize:
                case SerializedPropertyType.Character:
                case SerializedPropertyType.Bounds:
                case SerializedPropertyType.Quaternion:
                case SerializedPropertyType.Vector2Int:
                case SerializedPropertyType.Vector3Int:
                case SerializedPropertyType.RectInt:
                case SerializedPropertyType.BoundsInt:
                    return true;
                case SerializedPropertyType.ObjectReference:
                case SerializedPropertyType.Gradient:
                case SerializedPropertyType.ExposedReference:
                case SerializedPropertyType.ManagedReference:
                case SerializedPropertyType.FixedBufferSize:
                    return property.objectReferenceValue != null;
                case SerializedPropertyType.AnimationCurve:
                    return property.animationCurveValue != null;
            }

            return true;
        }

        public override string GetValidationError(ValidatePropertyAttribute attribute, SerializedProperty property)
        {
            string errorMessage = ((NotNullFieldAttribute)attribute).errorMessage;

            if (string.IsNullOrEmpty(errorMessage))
                return "NotNullField: Value is null";

            return errorMessage;
        }
    }
}