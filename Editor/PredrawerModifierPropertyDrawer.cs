﻿using UnityEditor;
using UnityEngine;

namespace Module.Inspector.Editor
{
    public abstract class PredrawerModifierPropertyDrawer : AbstractPropertyDrawer
    {
        public abstract void Modify(PredrawerModifierPropertyAttribute attribute, SerializedProperty property, GUIContent label);
    }
}