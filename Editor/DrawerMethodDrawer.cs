﻿using System.Reflection;
using UnityEngine;

namespace Module.Inspector.Editor
{
    public abstract class DrawerMethodDrawer : AbstractMethodDrawer
    {
        public abstract bool Draw(Rect position, DrawerMethodAttribute attribute, Object target, MethodInfo methodInfo);
        public abstract string GetErrorMessage();
    }
}