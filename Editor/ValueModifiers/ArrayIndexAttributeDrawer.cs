﻿using UnityEditor;
using UnityEngine;

namespace Module.Inspector.Editor
{
    [CustomPropertyDrawer(typeof(ArrayIndexAttribute))]
    internal sealed class ArrayIndexAttributeDrawer : ValueModifierPropertyDrawer
    {
        public override void Modify(ValueModifierPropertyAttribute attribute, SerializedProperty property)
        {
            var att = (ArrayIndexAttribute)attribute;

            int arraySize = GetSiblingArraySize(property, att);
            int max = arraySize > 0 ? arraySize - 1 : 0;
            int value = Mathf.Clamp(property.intValue, 0, max);

            if (value != property.intValue)
                property.intValue = value;
        }
        
        private static int GetSiblingArraySize(SerializedProperty sp, ArrayIndexAttribute att)
        {
            SerializedProperty spSibling = sp.GetSibling(att.fieldName);
            return spSibling != null && spSibling.isArray ? spSibling.arraySize : 0;
        }
    }
}