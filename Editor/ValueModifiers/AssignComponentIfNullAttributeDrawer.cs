﻿using System;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Module.Inspector.Editor
{
    [CustomPropertyDrawer(typeof(AssignComponentIfNullAttribute))]
    internal sealed class AssignComponentIfNullAttributeDrawer : ValueModifierPropertyDrawer
    {
        public override void Modify(ValueModifierPropertyAttribute attribute, SerializedProperty property)
        {
            if (property.propertyType != SerializedPropertyType.ObjectReference)
                return;
            if (property.objectReferenceValue != null)
                return;
            
            var att = (AssignComponentIfNullAttribute)attribute;
            Type type = att.useType ? att.type : property.GetValueType();

            if (!typeof(Component).IsAssignableFrom(type))
                return;

            for (var i = 0; i < property.serializedObject.targetObjects.Length; i++)
            {
                Object obj = property.serializedObject.targetObjects[i];

                if (!(obj is Component component))
                    continue;

                Component c = att.includeChildren 
                                  ? component.gameObject.GetComponentInChildren(type) 
                                  : component.gameObject.GetComponent(type);

                property.objectReferenceValue = c;
            }
        }
    }
}