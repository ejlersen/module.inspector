﻿using UnityEditor;

namespace Module.Inspector.Editor
{
    [CustomPropertyDrawer(typeof(LargerThanFieldAttribute))]
    internal sealed class LargerThanFieldAttributeDrawer : ValueModifierPropertyDrawer
    {
        public override void Modify(ValueModifierPropertyAttribute attribute, SerializedProperty property)
        {
            var att = (LargerThanFieldAttribute)attribute;
            SerializedPropertyExtension.ECompareType compareType = property.IsGreaterOrEqualToSiblingValue(att.fieldName);

            if (compareType == SerializedPropertyExtension.ECompareType.False)
                property.SetValueTo(property.GetSibling(att.fieldName));
        }
    }
}