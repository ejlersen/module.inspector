﻿using System;
using UnityEditor;
using Object = UnityEngine.Object;

namespace Module.Inspector.Editor
{
    [CustomPropertyDrawer(typeof(AssignAssetIfNullAttribute))]
    internal sealed class AssignAssetIfNullAttributeDrawer : ValueModifierPropertyDrawer
    {
        public override void Modify(ValueModifierPropertyAttribute attribute, SerializedProperty property)
        {
            if (property.propertyType != SerializedPropertyType.ObjectReference)
                return;
            if (property.objectReferenceValue != null)
                return;

            var att = (AssignAssetIfNullAttribute)attribute;
            Type type = att.useType ? att.type : property.GetValueType();
            property.objectReferenceValue = AssignAsset(type, att.filter, att.searchFolders);
        }

        private static Object AssignAsset(Type type, string filter, string[] folders)
        {
            string[] guids;
            
            if (folders != null && folders.Length != 0)
                guids = AssetDatabase.FindAssets($"{type.Name} {filter}", folders);
            else
                guids = AssetDatabase.FindAssets($"{type.Name} {filter}");

            for (var i = 0; i < guids.Length; i++)
            {
                string path = AssetDatabase.GUIDToAssetPath(guids[i]);
                Object obj = AssetDatabase.LoadAssetAtPath(path, type);

                if (obj != null)
                    return obj;
            }

            return null;
        }
    }
}