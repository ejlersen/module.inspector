﻿using UnityEditor;

namespace Module.Inspector.Editor
{
    [CustomPropertyDrawer(typeof(SmallerThanFieldAttribute))]
    internal sealed class SmallerThanFieldAttributeDrawer : ValueModifierPropertyDrawer
    {
        public override void Modify(ValueModifierPropertyAttribute attribute, SerializedProperty property)
        {
            var att = (SmallerThanFieldAttribute)attribute;
            SerializedPropertyExtension.ECompareType compareType = property.IsSmallerOrEqualToSiblingValue(att.fieldName);

            if (compareType == SerializedPropertyExtension.ECompareType.False)
                property.SetValueTo(property.GetSibling(att.fieldName));
        }
    }
}