﻿using UnityEditor;
using UnityEngine;

namespace Module.Inspector.Editor
{
    [CustomPropertyDrawer(typeof(SceneObjectsOnlyAttribute))]
    internal sealed class SceneObjectsOnlyAttributeDrawer : ValueModifierPropertyDrawer
    {
        public override void Modify(ValueModifierPropertyAttribute attribute, SerializedProperty property)
        {
            if (property.propertyType != SerializedPropertyType.ObjectReference)
                return;
            
            Object obj = property.objectReferenceValue;
            
            if (obj != null && !IsValid(obj))
                property.objectReferenceValue = null;
        }

        private static bool IsValid(Object obj)
        {
            return !AssetDatabase.Contains(obj);
        }
    }
}