﻿using UnityEditor;
using UnityEngine;

namespace Module.Inspector.Editor
{
    [CustomPropertyDrawer(typeof(MinValueAttribute))]
    internal sealed class MinValueAttributeDrawer : ValueModifierPropertyDrawer
    {
        public override void Modify(ValueModifierPropertyAttribute attribute, SerializedProperty property)
        {
            var min = (MinValueAttribute)attribute;

            switch (property.propertyType)
            {
                case SerializedPropertyType.Integer:
                    property.intValue = Mathf.Max(min.intValue, property.intValue);
                    break;
                case SerializedPropertyType.Float:
                    property.floatValue = Mathf.Max(min.floatValue, property.floatValue);
                    break;
                case SerializedPropertyType.Vector2:
                    Vector2 v2 = property.vector2Value;
                    v2.x = Mathf.Max(min.floatValue, property.vector2Value.x);
                    v2.y = Mathf.Max(min.floatValue, property.vector2Value.y);
                    property.vector2Value = v2;
                    break;
                case SerializedPropertyType.Vector3:
                    Vector3 v3 = property.vector3Value;
                    v3.x = Mathf.Max(min.floatValue, property.vector3Value.x);
                    v3.y = Mathf.Max(min.floatValue, property.vector3Value.y);
                    v3.z = Mathf.Max(min.floatValue, property.vector3Value.z);
                    property.vector3Value = v3;
                    break;
                case SerializedPropertyType.Vector4:
                    Vector4 v4 = property.vector4Value;
                    v4.x = Mathf.Max(min.floatValue, property.vector4Value.x);
                    v4.y = Mathf.Max(min.floatValue, property.vector4Value.y);
                    v4.z = Mathf.Max(min.floatValue, property.vector4Value.z);
                    v4.w = Mathf.Max(min.floatValue, property.vector4Value.w);
                    property.vector4Value = v4;
                    break;
                case SerializedPropertyType.Vector2Int:
                    Vector2Int v2Int = property.vector2IntValue;
                    v2Int.x = Mathf.Max(min.intValue, property.vector2IntValue.x);
                    v2Int.y = Mathf.Max(min.intValue, property.vector2IntValue.y);
                    property.vector2IntValue = v2Int;
                    break;
                case SerializedPropertyType.Vector3Int:
                    Vector3Int v3Int = property.vector3IntValue;
                    v3Int.x = Mathf.Max(min.intValue, property.vector3IntValue.x);
                    v3Int.y = Mathf.Max(min.intValue, property.vector3IntValue.y);
                    v3Int.z = Mathf.Max(min.intValue, property.vector3IntValue.z);
                    property.vector3IntValue = v3Int;
                    break;
            }
        }
    }
}