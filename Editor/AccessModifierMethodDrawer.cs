﻿using System.Reflection;
using UnityEngine;

namespace Module.Inspector.Editor
{
    public abstract class AccessModifierMethodDrawer : AbstractMethodDrawer
    {
        public abstract EAccessType GetAccessType(AccessModifierMethodAttribute attribute, Object target, MethodInfo methodInfo, EAccessType currentAccessType);
    }
}