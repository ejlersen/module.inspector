﻿using UnityEditor;

namespace Module.Inspector.Editor
{
    public abstract class AccessModifierPropertyDrawer : AbstractPropertyDrawer
    {
        public abstract EAccessType GetAccessType(AccessModifierPropertyAttribute attribute, SerializedProperty property, EAccessType currentAccessType);
    }
}