﻿using UnityEditor;
using UnityEngine;

namespace Module.Inspector.Editor
{
    [CustomPropertyDrawer(typeof(HorizontalLineAttribute))]
    public sealed class HorizontalLineAttributeDrawer : DecoratorDrawer
    {
        private const float LINE_HEIGHT = 1.0f;
        private const float VERTICAL_SPACING = 4.0f;
        
        public override void OnGUI(Rect position)
        {
            var att = (HorizontalLineAttribute)attribute;

            // Title
            if (!string.IsNullOrEmpty(att.title))
            {
                var style = new GUIStyle(GUI.skin.label)
                {
                    fontStyle = FontStyle.Bold
                };

                EditorGUI.LabelField(position, att.title, style);
            }

            // Line
            Color oldColor = GUI.color;
            GUI.color = GUI.skin.label.normal.textColor;
            var rect = new Rect(position.x, position.yMax - LINE_HEIGHT - VERTICAL_SPACING, position.width, LINE_HEIGHT);
            GUI.DrawTexture(rect, EditorGUIUtility.whiteTexture, ScaleMode.StretchToFill);
            GUI.color = oldColor;
        }

        public override float GetHeight()
        {
            return base.GetHeight() + VERTICAL_SPACING;
        }
    }
}