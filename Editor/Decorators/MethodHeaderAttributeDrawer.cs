﻿using UnityEditor;
using UnityEngine;

namespace Module.Inspector.Editor
{
    [CustomMethodDrawer(typeof(MethodHeaderAttribute))]
    public sealed class MethodHeaderAttributeDrawer : DecoratorMethodDrawer
    {
        public override bool Draw(Rect position, DecoratorMethodAttribute attribute)
        {
            var att = (MethodHeaderAttribute)attribute;
            
            if (string.IsNullOrEmpty(att.title))
                return false;

            var style = new GUIStyle(GUI.skin.label)
            {
                fontStyle = FontStyle.Bold,
                alignment = TextAnchor.LowerLeft
            };

            EditorGUI.LabelField(position, att.title, style);
            return true;
        }
        
        public override float GetHeight(DecoratorMethodAttribute attribute)
        {
            var att = (MethodHeaderAttribute)attribute;
            return !string.IsNullOrEmpty(att.title) ? EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing : 0.0f;
        }
    }
}