﻿using System;
using UnityEditor;
using UnityEngine;

namespace Module.Inspector.Editor
{
    [CustomPropertyDrawer(typeof(FieldLabelFromTypeAttribute))]
    internal sealed class FieldLabelFromTypeAttributeDrawer : PredrawerModifierPropertyDrawer
    {
        public override void Modify(PredrawerModifierPropertyAttribute attribute, SerializedProperty property, GUIContent label)
        {
            Type type = property.GetValueType();

            if (type != null)
                label.text = type.Name;
        }
    }
}