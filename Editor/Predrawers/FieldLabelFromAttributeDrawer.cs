﻿using UnityEditor;
using UnityEngine;

namespace Module.Inspector.Editor
{
    [CustomPropertyDrawer(typeof(FieldLabelFromAttribute))]
    internal sealed class FieldLabelFromAttributeDrawer : PredrawerModifierPropertyDrawer
    {
        public override void Modify(PredrawerModifierPropertyAttribute attribute, SerializedProperty property, GUIContent label)
        {
            var att = (FieldLabelFromAttribute)attribute;
            SerializedProperty sp = property.GetRelativeProperty(att.fieldName);

            if (sp == null)
                return;

            string str = sp.GetValueAsString();

            if (att.nicify)
                str = ObjectNames.NicifyVariableName(str);
            
            label.text = str;
        }
    }
}