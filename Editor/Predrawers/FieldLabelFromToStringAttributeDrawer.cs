﻿using UnityEditor;
using UnityEngine;

namespace Module.Inspector.Editor
{
    [CustomPropertyDrawer(typeof(FieldLabelFromToStringAttribute))]
    internal sealed class FieldLabelFromToStringAttributeDrawer : PredrawerModifierPropertyDrawer
    {
        public override void Modify(PredrawerModifierPropertyAttribute attribute, SerializedProperty property, GUIContent label)
        {
            label.text = property.GetValueAsString();
        }
    }
}