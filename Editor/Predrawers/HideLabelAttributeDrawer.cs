﻿using UnityEditor;
using UnityEngine;

namespace Module.Inspector.Editor
{
    [CustomPropertyDrawer(typeof(HideLabelAttribute))]
    internal sealed class HideLabelAttributeDrawer : PredrawerModifierPropertyDrawer
    {
        public override void Modify(PredrawerModifierPropertyAttribute attribute, SerializedProperty property, GUIContent label)
        {
            label.text = string.Empty;
        }
    }
}