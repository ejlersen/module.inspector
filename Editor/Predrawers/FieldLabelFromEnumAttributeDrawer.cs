﻿using System;
using UnityEditor;
using UnityEngine;

namespace Module.Inspector.Editor
{
    [CustomPropertyDrawer(typeof(FieldLabelFromEnumAttribute))]
    internal sealed class FieldLabelFromEnumAttributeDrawer : PredrawerModifierPropertyDrawer
    {
        public override void Modify(PredrawerModifierPropertyAttribute attribute, SerializedProperty property, GUIContent label)
        {
            var att = (FieldLabelFromEnumAttribute)attribute;

            if (!att.type.IsEnum)
                return;

            SerializedProperty sp = property.GetParent();

            if (sp == null)
                return;

            var index = sp.IndexOfProperty(property);

            if (index == -1)
                return;

            try
            {
                string str = Enum.GetName(att.type, index);

                if (att.nicify)
                    str = ObjectNames.NicifyVariableName(str);
            
                label.text = str;
            }
            catch (Exception)
            {
                // Ignore
            }
        }
    }
}