﻿using System;

namespace Module.Inspector.Editor
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true, Inherited = false)]
    internal sealed class CustomMethodDrawer : Attribute
    {
        internal readonly Type type;

        public CustomMethodDrawer(Type type)
        {
            this.type = type;
        }
    }
}