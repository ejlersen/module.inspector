﻿using Module.Inspector.Editor.Utilities;
using UnityEditor;
using UnityEngine;

namespace Module.Inspector.Editor
{
    [CustomPropertyDrawer(typeof(StringToTypeAttribute))]
    internal sealed class StringToTypeAttributeDrawer : DrawerPropertyDrawer
    {
        public override bool Draw(Rect position, DrawerPropertyAttribute attribute, SerializedProperty property, GUIContent label, EditorPropertyUtility.Result result)
        {
            if (property.propertyType != SerializedPropertyType.String)
                return false;
            
            var att = (StringToTypeAttribute)attribute;
            
            EditorGUI.BeginProperty(position, label, property);
            {
                const float WIDTH = 80;
                    
                var rect0 = new Rect(position.x, position.y, position.width - WIDTH, position.height);
                var rect1 = new Rect(rect0.xMax, position.y, WIDTH, position.height);
                string temp = EditorGUI.TextField(rect0, label, property.stringValue);

                if (!temp.Equals(property.stringValue))
                    property.stringValue = temp;

                if (GUI.Button(rect1, "Find"))
                    EditorWindowStringToType.Open(att.assignableFrom, property);
            }
            EditorGUI.EndProperty();
            return true;
        }
        
        public override string GetErrorMessage(SerializedProperty property)
        {
            return "Only supports strings";
        }
    }
}