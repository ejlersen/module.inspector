﻿using Module.Inspector.Editor.Utilities;
using UnityEditor;
using UnityEngine;

namespace Module.Inspector.Editor
{
    [CustomPropertyDrawer(typeof(PercentageAttribute))]
    internal sealed class PercentageAttributeDrawer : DrawerPropertyDrawer
    {
        public override bool Draw(Rect position, DrawerPropertyAttribute attribute, SerializedProperty property, GUIContent label, EditorPropertyUtility.Result result)
        {
            if (property.propertyType != SerializedPropertyType.Float)
                return false;
            
            EditorGUI.BeginChangeCheck();
            EditorGUI.BeginProperty(position, label, property);
            {
                float v = property.floatValue * 100.0f;
                string str = EditorGUI.TextField(position, label, v.ToString("0.0") + "%");
                str = str.Replace('%', ' ').Trim();

                if (float.TryParse(str, out float temp))
                    v = temp;
                    
                v *= 0.01f;
                property.floatValue = v;
            }
            EditorGUI.EndProperty();
            bool changed = EditorGUI.EndChangeCheck();
                
            if (changed)
                property.serializedObject.ApplyModifiedProperties();
            
            return true;
        }
        
        public override string GetErrorMessage(SerializedProperty property)
        {
            return "Only supports float";
        }
    }
}