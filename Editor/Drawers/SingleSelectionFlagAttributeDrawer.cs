﻿using Module.Inspector.Editor.Utilities;
using UnityEditor;
using UnityEngine;

namespace Module.Inspector.Editor
{
    [CustomPropertyDrawer(typeof(SingleSelectionFlagAttribute))]
    internal sealed class SingleSelectionFlagAttributeDrawer : DrawerPropertyDrawer
    {
        public override bool Draw(Rect position, DrawerPropertyAttribute attribute, SerializedProperty property, GUIContent label, EditorPropertyUtility.Result result)
        {
            if (property.propertyType != SerializedPropertyType.Enum)
                return false;
            
            EditorGUI.BeginProperty(position, label, property);
            {
                property.enumValueIndex = EditorGUI.Popup(position, label.text, property.enumValueIndex, property.enumDisplayNames);
            }
            EditorGUI.EndProperty();
            return true;
        }
        
        public override string GetErrorMessage(SerializedProperty property)
        {
            return "Only supports enum";
        }
    }
}