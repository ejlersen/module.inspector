﻿using System;
using System.Collections.Generic;
using Module.Inspector.Editor.Utilities;
using UnityEditor;
using UnityEngine;

namespace Module.Inspector.Editor
{
    [CustomPropertyDrawer(typeof(SceneDropdownAttribute))]
    internal sealed class SceneDropdownAttributeDrawer : DrawerPropertyDrawer
    {
        private static GUIContent[] NAMES;
        private static string[] PATHS;
        
        public override bool Draw(Rect position, DrawerPropertyAttribute attribute, SerializedProperty property, GUIContent label, EditorPropertyUtility.Result result)
        {
            if (property.propertyType != SerializedPropertyType.String)
                return false;
            
            FetchScenes();
                
            EditorGUI.BeginChangeCheck();
            EditorGUI.BeginProperty(position, label, property);
            {
                int index = Array.IndexOf(PATHS, property.stringValue);
                int newIndex = EditorGUI.Popup(position, label, index, NAMES);

                if (newIndex != -1 && index != newIndex)
                    property.stringValue = PATHS[newIndex];
            }
            EditorGUI.EndProperty();
            bool changed = EditorGUI.EndChangeCheck();

            if (changed)
                property.serializedObject.ApplyModifiedProperties();
            
            return true;
        }
        
        public override string GetErrorMessage(SerializedProperty property)
        {
            return "Only supports strings";
        }

        private static string ToSceneName(string path)
        {
            return !string.IsNullOrEmpty(path) ? path.Substring(7, path.Length - 13).Replace('/', '\\') : string.Empty;
        }

        private static void FetchScenes()
        {
            if (NAMES != null)
                return;

            EditorBuildSettingsScene[] scenes = EditorBuildSettings.scenes;
            var listNames = new List<GUIContent>(scenes.Length);
            var listPaths = new List<string>(scenes.Length);
            
            for (var i = 0; i < scenes.Length; i++)
            {
                string path = scenes[i].path;
                
                if (string.IsNullOrEmpty(path))
                    continue;
                
                listNames.Add(new GUIContent(ToSceneName(path)));
                listPaths.Add(path);
            }

            NAMES = listNames.ToArray();
            PATHS = listPaths.ToArray();
        }
    }
}