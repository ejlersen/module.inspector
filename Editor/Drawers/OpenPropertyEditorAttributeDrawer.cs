﻿using Module.Inspector.Editor.Utilities;
using UnityEditor;
using UnityEngine;

namespace Module.Inspector.Editor
{
    [CustomPropertyDrawer(typeof(OpenPropertyEditorAttribute))]
    internal sealed class OpenPropertyEditorAttributeDrawer : DrawerPropertyDrawer
    {
        public override bool Draw(Rect position, DrawerPropertyAttribute attribute, SerializedProperty property, GUIContent label, EditorPropertyUtility.Result result)
        {
            if (!IsSupported(property))
                return false;
            
            EditorGUI.BeginProperty(position, label, property);
            {
                const float WIDTH = 50;
                    
                var rect0 = new Rect(position.x, position.y, position.width - WIDTH, position.height);
                var rect1 = new Rect(rect0.xMax, position.y, WIDTH, position.height);
                EditorGUI.PropertyField(rect0, property, label);

                var prevValue = GUI.enabled;
                GUI.enabled = prevValue && property.objectReferenceValue != null;
                
                if (GUI.Button(rect1, "Show"))
                    EditorUtility.OpenPropertyEditor(property.objectReferenceValue);

                GUI.enabled = prevValue;
            }
            EditorGUI.EndProperty();
            return true;
        }

        public override string GetErrorMessage(SerializedProperty property)
        {
            return IsSupported(property)
                ? "Unknown error"
                : "Only supports types which inherits from ScriptableObject/Component type";
        }
        
        private static bool IsSupported(SerializedProperty property)
        {
            return property.propertyType == SerializedPropertyType.ObjectReference &&
                   (typeof(ScriptableObject).IsAssignableFrom(property.GetValueType()) ||
                    typeof(Component).IsAssignableFrom(property.GetValueType()));
        }
    }
}