﻿using System.Reflection;
using UnityEngine;

namespace Module.Inspector.Editor
{
    [CustomMethodDrawer(typeof(MethodButtonAttribute))]
    internal sealed class MethodButtonAttributeDrawer : DrawerMethodDrawer
    {
        public override bool Draw(Rect position, DrawerMethodAttribute attribute, Object target, MethodInfo methodInfo)
        {
            var att = (MethodButtonAttribute)attribute;
            string name = string.IsNullOrEmpty(att.name) ? methodInfo.Name : att.name;

            if (GUI.Button(position, name))
                methodInfo.Invoke(methodInfo.IsStatic ? null : target, null);

            return true;
        }

        public override string GetErrorMessage()
        {
            return string.Empty;
        }
    }
}