﻿using Module.Inspector.Editor.Utilities;
using UnityEditor;
using UnityEngine;

namespace Module.Inspector.Editor
{
    [CustomPropertyDrawer(typeof(FolderPathAttribute))]
    internal sealed class FolderPathAttributeDrawer : DrawerPropertyDrawer
    {
        public override bool Draw(Rect position, DrawerPropertyAttribute attribute, SerializedProperty property, GUIContent label, EditorPropertyUtility.Result result)
        {
            if (property.propertyType != SerializedPropertyType.String)
                return false;
            
            var att = (FolderPathAttribute)attribute;
            
            EditorGUI.BeginProperty(position, label, property);
            {
                const float WIDTH = 80;
                    
                var rect0 = new Rect(position.x, position.y, position.width - WIDTH, position.height);
                var rect1 = new Rect(rect0.xMax, position.y, WIDTH, position.height);
                string temp = EditorGUI.TextField(rect0, label, property.stringValue);

                if (!temp.Equals(property.stringValue))
                    property.stringValue = temp;
                
                if (GUI.Button(rect1, "Find"))
                {
                    string path = EditorUtility.OpenFolderPanel("Folder", "Assets/", string.Empty);

                    if (!string.IsNullOrEmpty(path))
                    {
                        if (!att.useAbsolute && path.StartsWith(Application.dataPath))
                            path = path.Remove(0, Application.dataPath.Length - 6);

                        property.stringValue = path;
                        property.serializedObject.ApplyModifiedProperties();
                    }
                }
            }
            EditorGUI.EndProperty();
            return true;
        }
        
        public override string GetErrorMessage(SerializedProperty property)
        {
            return "Only supports strings";
        }
    }
}