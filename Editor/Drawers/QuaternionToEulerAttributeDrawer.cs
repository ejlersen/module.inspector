﻿#if !UNITY_2021_1_OR_NEWER
using Module.Inspector.Editor.Utilities;
using UnityEditor;
using UnityEngine;

namespace Module.Inspector.Editor
{
    [CustomPropertyDrawer(typeof(QuaternionToEulerAttribute))]
    internal sealed class QuaternionToEulerAttributeDrawer : DrawerPropertyDrawer
    {
        public override bool Draw(Rect position, DrawerPropertyAttribute attribute, SerializedProperty property, GUIContent label, EditorPropertyUtility.Result result)
        {
            if (property.propertyType != SerializedPropertyType.Quaternion)
                return false;
            
            EditorGUI.BeginChangeCheck();
            EditorGUI.BeginProperty(position, label, property);
            {
                Vector3 euler = property.quaternionValue.eulerAngles;
                Vector3 temp = EditorGUI.Vector3Field(position, label, euler);
                property.quaternionValue = Quaternion.Euler(temp);
            }
            EditorGUI.EndProperty();
            bool changed = EditorGUI.EndChangeCheck();

            if (changed)
                property.serializedObject.ApplyModifiedProperties();
            
            return true;
        }
        
        public override string GetErrorMessage(SerializedProperty property)
        {
            return "Only supports quaternions";
        }
    }
}
#endif