﻿using System;
using Module.Inspector.Editor.Utilities;
using UnityEditor;
using UnityEngine;

namespace Module.Inspector.Editor
{
    [CustomPropertyDrawer(typeof(PopupFromScriptableObjectTypeAttribute))]
    internal sealed class PopupFromScriptableObjectTypeAttributeDrawer : DrawerPropertyDrawer
    {
        private static readonly Type ASSIGNABLE_FROM_TYPE = typeof(ScriptableObject);
        
        public override bool Draw(Rect position, DrawerPropertyAttribute attribute, SerializedProperty property, GUIContent label, EditorPropertyUtility.Result result)
        {
            if (property.propertyType != SerializedPropertyType.ObjectReference)
                return false;

            var att = (PopupFromScriptableObjectTypeAttribute)attribute;
            Type targetType = att.type != null ? att.type : property.GetValueType();
            
            if (targetType != null && !ASSIGNABLE_FROM_TYPE.IsAssignableFrom(targetType))
                return false;
            
            EditorGUI.BeginChangeCheck();
            EditorGUI.BeginProperty(position, label, property);
            {
                const float BUTTON_WIDTH = 20f;
                const float BUTTON_PADDING = 3f;
                
                var rect0 = new Rect(position.x, position.y, position.width - BUTTON_WIDTH, position.height);
                var rect1 = new Rect(rect0.xMax + BUTTON_PADDING, rect0.y + 1, BUTTON_WIDTH - BUTTON_PADDING, rect0.height - 3);
                
                EditorUtilitySearchObject.SearchObject searchObject = EditorUtilitySearchObject.SearchAndCache(targetType);
                int index = searchObject.assets.IndexOf(property.objectReferenceValue) + 1;
                int newIndex = EditorGUI.Popup(rect0, label, index, searchObject.names);

                if (newIndex != -1 && index != newIndex)
                    property.objectReferenceValue = newIndex > 0 ? searchObject.assets[newIndex - 1] : null;
                
                if (GUI.Button(rect1, EditorIcons.GetScriptableObjectIconContent(), GUIStyle.none) && property.objectReferenceValue != null)
                    EditorGUIUtility.PingObject(property.objectReferenceValue);
            }
            EditorGUI.EndProperty();
            bool changed = EditorGUI.EndChangeCheck();

            if (changed)
                property.serializedObject.ApplyModifiedProperties();
            
            return true;
        }
        
        public override string GetErrorMessage(SerializedProperty property)
        {
            return "Only supports Scriptable Object types";
        }
    }
}