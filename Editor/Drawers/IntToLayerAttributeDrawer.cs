﻿using System;
using System.Collections.Generic;
using Module.Inspector.Editor.Utilities;
using UnityEditor;
using UnityEngine;

namespace Module.Inspector.Editor
{
    [CustomPropertyDrawer(typeof(IntToLayerAttribute))]
    internal sealed class IntToLayerAttributeDrawer : DrawerPropertyDrawer
    {
        private static GUIContent[] NAMES;
        private static int[] INDICES;
        
        public override bool Draw(Rect position, DrawerPropertyAttribute attribute, SerializedProperty property, GUIContent label, EditorPropertyUtility.Result result)
        {
            if (property.propertyType != SerializedPropertyType.Integer)
                return false;
            
            FetchLayerNames();
                
            EditorGUI.BeginChangeCheck();
            EditorGUI.BeginProperty(position, label, property);
            {
                int index = Array.IndexOf(INDICES, property.intValue);
                int newIndex = EditorGUI.Popup(position, label, index, NAMES);

                if (newIndex != -1 && index != newIndex)
                    property.intValue = INDICES[newIndex];
            }
            EditorGUI.EndProperty();
            bool changed = EditorGUI.EndChangeCheck();

            if (changed)
                property.serializedObject.ApplyModifiedProperties();
            
            return true;
        }
        
        public override string GetErrorMessage(SerializedProperty property)
        {
            return "Only supports integers";
        }

        private static void FetchLayerNames()
        {
            if (NAMES != null)
                return;
            
            var listNames = new List<GUIContent>(32);
            var listIndices = new List<int>(32);

            for (var i = 0; i < 32; i++)
            {
                string name = LayerMask.LayerToName(i);
                
                if (string.IsNullOrEmpty(name))
                    continue;
                
                listNames.Add(new GUIContent(name));
                listIndices.Add(i);
            }

            NAMES = listNames.ToArray();
            INDICES = listIndices.ToArray();
        }
    }
}