﻿using System;
using Module.Inspector.Editor.Utilities;
using UnityEditor;
using UnityEngine;

namespace Module.Inspector.Editor
{
    [CustomPropertyDrawer(typeof(EnumFlagAttribute))]
    internal sealed class EnumFlagAttributeDrawer : DrawerPropertyDrawer
    {
        public override bool Draw(Rect position, DrawerPropertyAttribute attribute, SerializedProperty property, GUIContent label, EditorPropertyUtility.Result result)
        {
            if (property.propertyType != SerializedPropertyType.Enum)
                return false;

            Type type = property.GetValueType();
            
            EditorGUI.BeginProperty(position, label, property);
            {
                var e = (Enum)Enum.ToObject(type, property.intValue);
                e = EditorGUI.EnumFlagsField(position, label, e);
                property.intValue = Convert.ToInt32(e);
            }
            EditorGUI.EndProperty();
            return true;
        }
        
        public override string GetErrorMessage(SerializedProperty property)
        {
            return "Only supports enum";
        }
    }
}