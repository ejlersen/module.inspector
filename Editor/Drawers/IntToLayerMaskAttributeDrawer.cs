﻿using Module.Inspector.Editor.Utilities;
using UnityEditor;
using UnityEngine;

namespace Module.Inspector.Editor
{
    [CustomPropertyDrawer(typeof(IntToLayerMaskAttribute))]
    internal sealed class IntToLayerMaskAttributeDrawer : DrawerPropertyDrawer
    {
        private static string[] NAMES;
        
        public override bool Draw(Rect position, DrawerPropertyAttribute attribute, SerializedProperty property, GUIContent label, EditorPropertyUtility.Result result)
        {
            if (property.propertyType != SerializedPropertyType.Integer)
                return false;
            
            FetchLayerNames();
            
            EditorGUI.BeginChangeCheck();
            EditorGUI.BeginProperty(position, label, property);
            {
                property.intValue = EditorGUI.MaskField(position, label, property.intValue, NAMES);
            }
            EditorGUI.EndProperty();
            bool changed = EditorGUI.EndChangeCheck();

            if (changed)
                property.serializedObject.ApplyModifiedProperties();
            
            return true;
        }
        
        public override string GetErrorMessage(SerializedProperty property)
        {
            return "Only supports integers";
        }
        
        
        private static void FetchLayerNames()
        {
            if (NAMES != null)
                return;

            NAMES = new string[32];

            for (var i = 0; i < 32; i++)
            {
                string str = LayerMask.LayerToName(i);

                if (string.IsNullOrEmpty(str))
                    str = $"Unused Layer #{i:00}";
                
                NAMES[i] = str;
            }
        }
    }
}