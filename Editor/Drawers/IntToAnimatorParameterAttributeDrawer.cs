﻿using System.Collections.Generic;
using Module.Inspector.Editor.Utilities;
using UnityEditor;
using UnityEngine;

namespace Module.Inspector.Editor
{
    [CustomPropertyDrawer(typeof(IntToAnimatorParameterAttribute))]
    internal sealed class IntToAnimatorParameterAttributeDrawer : DrawerPropertyDrawer
    {
        private readonly List<string> names = new List<string>();
        private readonly List<int> hashes = new List<int>();
        private GUIContent[] contentArr;
        
        public override bool Draw(Rect position, DrawerPropertyAttribute attribute, SerializedProperty property, GUIContent label, EditorPropertyUtility.Result result)
        {
            if (property.propertyType != SerializedPropertyType.Integer)
                return false;
            
            var att = (IntToAnimatorParameterAttribute)attribute;
            SerializedProperty spSibling = property.GetSibling(att.animatorFieldName);

            if (spSibling == null)
                return false;
            
            FetchParameters(spSibling);
                
            EditorGUI.BeginChangeCheck();
            EditorGUI.BeginProperty(position, label, property);
            {
                int index = hashes.IndexOf(property.intValue);

                if (index < 0)
                    index = 0;
                
                int newIndex = EditorGUI.Popup(position, label, index, contentArr);
                property.intValue = newIndex >= 1 ? hashes[newIndex] : -1;
            }
            EditorGUI.EndProperty();
            bool changed = EditorGUI.EndChangeCheck();

            if (changed)
                property.serializedObject.ApplyModifiedProperties();
            
            return true;
        }
        
        public override string GetErrorMessage(SerializedProperty property)
        {
            if (property.propertyType != SerializedPropertyType.Integer)
                return "Only supports integers";
            
            var att = (IntToAnimatorParameterAttribute)attribute;
            return $"Missing field: {att.animatorFieldName}";
        }
        
        private void FetchParameters(SerializedProperty property)
        {
            var animator = property.objectReferenceValue as Animator;
            
            names.Clear();
            names.Add("----");
            
            hashes.Clear();
            hashes.Add(0);

            if (animator != null)
            {
                for (var i = 0; i < animator.parameterCount; i++)
                {
                    names.Add(animator.parameters[i].name);
                    hashes.Add(Animator.StringToHash(animator.parameters[i].name));
                }
            }

            contentArr = new GUIContent[names.Count];

            for (var i = 0; i < contentArr.Length; i++)
            {
                contentArr[i] = new GUIContent(names[i]);
            }
        }
    }
}