﻿using System.Collections.Generic;
using Module.Inspector.Editor.Utilities;
using UnityEditor;
using UnityEngine;

namespace Module.Inspector.Editor
{
    [CustomPropertyDrawer(typeof(StringToAnimatorParameterAttribute))]
    internal sealed class StringToAnimatorParameterAttributeDrawer : DrawerPropertyDrawer
    {
        private readonly List<string> names = new List<string>();
        private GUIContent[] contentArr;
        
        public override bool Draw(Rect position, DrawerPropertyAttribute attribute, SerializedProperty property, GUIContent label, EditorPropertyUtility.Result result)
        {
            if (property.propertyType != SerializedPropertyType.String)
                return false;
            
            var att = (StringToAnimatorParameterAttribute)attribute;
            SerializedProperty spSibling = property.GetSibling(att.animatorFieldName);

            if (spSibling == null)
                return false;
            
            FetchParameters(spSibling);
                
            EditorGUI.BeginChangeCheck();
            EditorGUI.BeginProperty(position, label, property);
            {
                int index = names.IndexOf(property.stringValue);

                if (index < 0)
                    index = 0;
                
                int newIndex = EditorGUI.Popup(position, label, index, contentArr);
                property.stringValue = newIndex >= 1 ? names[newIndex] : string.Empty;
            }
            EditorGUI.EndProperty();
            bool changed = EditorGUI.EndChangeCheck();

            if (changed)
                property.serializedObject.ApplyModifiedProperties();
            
            return true;
        }
        
        public override string GetErrorMessage(SerializedProperty property)
        {
            if (property.propertyType != SerializedPropertyType.String)
                return "Only supports strings";

            var att = (StringToAnimatorParameterAttribute)attribute;
            return $"Missing field: {att.animatorFieldName}";
        }
        
        private void FetchParameters(SerializedProperty property)
        {
            var animator = property.objectReferenceValue as Animator;
            names.Clear();
            names.Add("----");

            if (animator != null)
            {
                for (var i = 0; i < animator.parameterCount; i++)
                {
                    names.Add(animator.parameters[i].name);
                }
            }

            contentArr = new GUIContent[names.Count];

            for (var i = 0; i < contentArr.Length; i++)
            {
                contentArr[i] = new GUIContent(names[i]);
            }
        }
    }
}