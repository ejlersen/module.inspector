using Module.Inspector.Editor.Utilities;
using UnityEditor;
using UnityEngine;

namespace Module.Inspector.Editor
{
    [CustomPropertyDrawer(typeof(UrlGoToAttribute))]
    internal sealed class UrlGoToAttributeDrawer : DrawerPropertyDrawer
    {
        public override bool Draw(Rect position, DrawerPropertyAttribute attribute, SerializedProperty property, GUIContent label, EditorPropertyUtility.Result result)
        {
            if (property.propertyType != SerializedPropertyType.String)
                return false;
            
            EditorGUI.BeginProperty(position, label, property);
            {
                const float WIDTH = 80;
                
                var r0 = new Rect(position.x, position.y, position.width - WIDTH, position.height);
                var r1 = new Rect(r0.xMax, r0.y, WIDTH, r0.height);
                string temp = EditorGUI.TextField(r0, label, property.stringValue);

                if (!temp.Equals(property.stringValue))
                    property.stringValue = temp;
                
                if (GUI.Button(r1, "Go"))
                    Application.OpenURL(property.stringValue);
            }
            EditorGUI.EndProperty();
            return true;
        }

        public override string GetErrorMessage(SerializedProperty property)
        {
            return "Only supports strings";
        }
    }
}