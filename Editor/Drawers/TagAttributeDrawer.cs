﻿using System;
using Module.Inspector.Editor.Utilities;
using UnityEditor;
using UnityEngine;

namespace Module.Inspector.Editor
{
    [CustomPropertyDrawer(typeof(TagAttribute))]
    internal sealed class TagAttributeDrawer : DrawerPropertyDrawer
    {
        private string[] names;
        private GUIContent[] contentArr;
        
        public override bool Draw(Rect position, DrawerPropertyAttribute attribute, SerializedProperty property, GUIContent label, EditorPropertyUtility.Result result)
        {
            if (property.propertyType != SerializedPropertyType.String)
                return false;
            
            FetchTags();
                
            EditorGUI.BeginChangeCheck();
            EditorGUI.BeginProperty(position, label, property);
            {
                int index = Array.IndexOf(names, property.stringValue);
                int newIndex = EditorGUI.Popup(position, label, index, contentArr);
                property.stringValue = newIndex >= 0 ? names[newIndex] : names[0];
            }
            EditorGUI.EndProperty();
            bool changed = EditorGUI.EndChangeCheck();

            if (changed)
                property.serializedObject.ApplyModifiedProperties();
            
            return true;
        }
        
        public override string GetErrorMessage(SerializedProperty property)
        {
            return "Only supports strings";
        }
        
        private void FetchTags()
        {
            names = UnityEditorInternal.InternalEditorUtility.tags;
            contentArr = new GUIContent[names.Length];

            for (var i = 0; i < contentArr.Length; i++)
            {
                contentArr[i] = new GUIContent(names[i]);
            }
        }
    }
}