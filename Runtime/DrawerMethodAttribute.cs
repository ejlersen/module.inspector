﻿namespace Module.Inspector
{
    public abstract class DrawerMethodAttribute : AbstractMethodAttribute
    {
        public override EMethodType Type => EMethodType.Drawer;
    }
}