﻿namespace Module.Inspector
{
    public enum EPropertyType : byte
    {
        PredrawerModifier,
        Drawer,
        ValueModifier,
        AccessModifier,
        HandleDrawer,
        Validate
    }
}