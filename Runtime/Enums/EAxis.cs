﻿using System;

namespace Module.Inspector
{
    [Flags]
    public enum EAxis
    {
        None = 0,
        X    = 1,
        Y    = 2,
        Z    = 4,
        All  = X | Y | Z
    }
}