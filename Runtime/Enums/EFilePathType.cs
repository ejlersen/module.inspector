﻿namespace Module.Inspector
{
    public enum EFilePathType
    {
        RelativeToProjectFolder,
        RelativeToStreamingFolder,
        Absolute
    }
}