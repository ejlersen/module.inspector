﻿using System;

namespace Module.Inspector
{
    [Flags]
    public enum ELabelType
    {
        None  = 0,
        Field = 1,
        Value = 2
    }
}