﻿namespace Module.Inspector
{
    public enum EMethodType : byte
    {
        Drawer,
        AccessModifier,
        Decorator
    }
}