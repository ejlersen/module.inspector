﻿namespace Module.Inspector
{
    public abstract class DrawerPropertyAttribute : AbstractPropertyAttribute
    {
        public override EPropertyType Type => EPropertyType.Drawer;
    }
}