﻿namespace Module.Inspector
{
    public abstract class HandleDrawerPropertyAttribute : AbstractPropertyAttribute
    {
        public override EPropertyType Type => EPropertyType.HandleDrawer;
    }
}