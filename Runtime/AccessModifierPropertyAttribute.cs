﻿namespace Module.Inspector
{
    public abstract class AccessModifierPropertyAttribute : AbstractPropertyAttribute
    {
        public override EPropertyType Type => EPropertyType.AccessModifier;
    }
}