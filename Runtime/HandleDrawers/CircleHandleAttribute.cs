﻿using System;
using UnityEngine;

namespace Module.Inspector
{
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public sealed class CircleHandleAttribute : HandleDrawerPropertyAttribute
    {
        public readonly string fieldPosition;
        public readonly string fieldRotation;
        public readonly Space space;

        public CircleHandleAttribute(string fieldPosition = null,
                                     string fieldRotation = null,
                                     Space space = Space.World)
        {
            this.fieldPosition = fieldPosition;
            this.fieldRotation = fieldRotation;
            this.space = space;
        }
    }
}