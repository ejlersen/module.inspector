﻿using System;
using UnityEngine;

namespace Module.Inspector
{
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public sealed class LabelHandleAttribute : HandleDrawerPropertyAttribute
    {
        public readonly ELabelType type;
        public readonly string fieldPosition;
        public readonly Space space;

        public LabelHandleAttribute(ELabelType type = ELabelType.Value, 
                                    string fieldPosition = null, 
                                    Space space = Space.World)
        {
            this.type = type;
            this.fieldPosition = fieldPosition;
            this.space = space;
        }
    }
}