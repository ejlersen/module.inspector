﻿using System;
using UnityEngine;

namespace Module.Inspector
{
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public sealed class RotationHandleAttribute : HandleDrawerPropertyAttribute
    {
        public readonly string fieldPosition;
        public readonly Space space;

        public RotationHandleAttribute(string fieldPosition = null, Space space = Space.World)
        {
            this.fieldPosition = fieldPosition;
            this.space = space;
        }
    }
}