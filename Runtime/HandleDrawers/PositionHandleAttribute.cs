﻿using System;
using UnityEngine;

namespace Module.Inspector
{
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public sealed class PositionHandleAttribute : HandleDrawerPropertyAttribute
    {
        public readonly Space space;
        public readonly EAxis axiis;

        public PositionHandleAttribute(Space space, EAxis axiis)
        {
            this.space = space;
            this.axiis = axiis;
        }
    }
}