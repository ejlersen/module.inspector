using System;

namespace Module.Inspector
{
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public sealed class AssignComponentIfNullAttribute : ValueModifierPropertyAttribute
    {
        public readonly bool includeChildren;
        public readonly bool useType;
        public readonly Type type;

        public AssignComponentIfNullAttribute(bool includeChildren = false)
        {
            this.includeChildren = includeChildren;
            useType = false;
        }
        
        public AssignComponentIfNullAttribute(Type type, bool includeChildren = false)
        {
            this.type = type;
            this.includeChildren = includeChildren;
            useType = true;
        }
    }
}