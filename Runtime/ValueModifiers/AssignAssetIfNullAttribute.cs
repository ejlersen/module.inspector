using System;

namespace Module.Inspector
{
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public sealed class AssignAssetIfNullAttribute : ValueModifierPropertyAttribute
    {
        public readonly bool useType;
        public readonly Type type;
        public readonly string filter;
        public readonly string[] searchFolders;

        public AssignAssetIfNullAttribute(string[] searchFolders = null)
        {
            this.searchFolders = searchFolders;
        }
        
        public AssignAssetIfNullAttribute(string filter, string[] searchFolders = null)
        {
            this.filter = filter;
            this.searchFolders = searchFolders;
        }
        
        public AssignAssetIfNullAttribute(Type type, string[] searchFolders = null)
        {
            this.type = type;
            this.searchFolders = searchFolders;
            useType = true;
        }
        
        public AssignAssetIfNullAttribute(Type type, string filter, string[] searchFolders = null)
        {
            this.type = type;
            this.filter = filter;
            this.searchFolders = searchFolders;
            useType = true;
        }
    }
}