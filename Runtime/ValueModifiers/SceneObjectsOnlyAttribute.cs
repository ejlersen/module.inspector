﻿using System;
using UnityEngine.Scripting;

namespace Module.Inspector
{
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public sealed class SceneObjectsOnlyAttribute : ValueModifierPropertyAttribute
    {
        [Preserve]
        public SceneObjectsOnlyAttribute()
        {
        }
    }
}