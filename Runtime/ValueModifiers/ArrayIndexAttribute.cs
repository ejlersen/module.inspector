﻿using System;

namespace Module.Inspector
{
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public sealed class ArrayIndexAttribute : ValueModifierPropertyAttribute
    {
        public readonly string fieldName;

        public ArrayIndexAttribute(string fieldName)
        {
            this.fieldName = fieldName;
        }
    }
}