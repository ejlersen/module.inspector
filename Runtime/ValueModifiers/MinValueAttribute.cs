﻿using System;
using UnityEngine;

namespace Module.Inspector
{
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]   
    public sealed class MinValueAttribute : ValueModifierPropertyAttribute
    {
        public readonly int intValue;
        public readonly float floatValue;

        public MinValueAttribute(int value)
        {
            intValue = value;
            floatValue = value;
        }

        public MinValueAttribute(float value)
        {
            intValue = Mathf.RoundToInt(value);
            floatValue = value;
        }
    }
}