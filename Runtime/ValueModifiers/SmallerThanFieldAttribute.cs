﻿using System;

namespace Module.Inspector
{
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public sealed class SmallerThanFieldAttribute : ValueModifierPropertyAttribute
    {
        public readonly string fieldName;

        public SmallerThanFieldAttribute(string fieldName)
        {
            this.fieldName = fieldName;
        }
    }
}