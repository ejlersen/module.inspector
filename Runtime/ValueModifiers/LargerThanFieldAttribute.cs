﻿using System;

namespace Module.Inspector
{
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public sealed class LargerThanFieldAttribute : ValueModifierPropertyAttribute
    {
        public readonly string fieldName;

        public LargerThanFieldAttribute(string fieldName)
        {
            this.fieldName = fieldName;
        }
    }
}