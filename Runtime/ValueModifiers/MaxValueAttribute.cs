﻿using System;
using UnityEngine;

namespace Module.Inspector
{
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public sealed class MaxValueAttribute : ValueModifierPropertyAttribute
    {
        public readonly int intValue;
        public readonly float floatValue;

        public MaxValueAttribute(int value)
        {
            intValue = value;
            floatValue = value;
        }

        public MaxValueAttribute(float value)
        {
            intValue = Mathf.RoundToInt(value);
            floatValue = value;
        }
    }
}