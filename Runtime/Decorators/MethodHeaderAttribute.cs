﻿using System;
using UnityEngine.Scripting;

namespace Module.Inspector
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public sealed class MethodHeaderAttribute : DecoratorMethodAttribute
    {
        public readonly string title;

        [Preserve]
        public MethodHeaderAttribute()
        {
        }

        public MethodHeaderAttribute(string title)
        {
            this.title = title;
        }
    }
}