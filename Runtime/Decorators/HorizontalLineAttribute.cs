﻿using System;
using UnityEngine;
using UnityEngine.Scripting;

namespace Module.Inspector
{
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public sealed class HorizontalLineAttribute : PropertyAttribute
    {
        public readonly string title;

        [Preserve]
        public HorizontalLineAttribute()
        {
        }

        public HorizontalLineAttribute(string title)
        {
            this.title = title;
        }
    }
}