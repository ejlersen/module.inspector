﻿using System;
using UnityEngine;

namespace Module.Inspector
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = true)]
    public sealed class EnableShowHiddenFieldsAttribute : PropertyAttribute
    {
        public bool UseFieldProperty { get; }
        
        public EnableShowHiddenFieldsAttribute(bool useFieldProperty = false)
        {
            UseFieldProperty = useFieldProperty;
        }
    }
}