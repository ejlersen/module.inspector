﻿using System;
using UnityEngine;

namespace Module.Inspector
{
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public sealed class ShowHiddenFieldAttribute : PropertyAttribute
    {
        public readonly bool editable;

        public ShowHiddenFieldAttribute(bool editable = false)
        {
            this.editable = editable;
        }
    }
}