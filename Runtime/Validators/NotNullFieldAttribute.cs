﻿using System;
using UnityEngine.Scripting;

namespace Module.Inspector
{
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public class NotNullFieldAttribute : ValidatePropertyAttribute
    {
        public readonly string errorMessage;
        
        [Preserve]
        public NotNullFieldAttribute()
        {
        }

        public NotNullFieldAttribute(string errorMessage)
        {
            this.errorMessage = errorMessage;
        }
    }
}