﻿namespace Module.Inspector
{
    public abstract class ValidatePropertyAttribute : AbstractPropertyAttribute
    {
        public override EPropertyType Type => EPropertyType.Validate;
    }
}