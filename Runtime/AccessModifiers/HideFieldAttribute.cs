﻿using System;
using UnityEngine.Scripting;

namespace Module.Inspector
{
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public sealed class HideFieldAttribute : AccessModifierPropertyAttribute
    {
        public readonly string fieldName;
        public readonly bool useFieldValue;
        public readonly object fieldValue;

        [Preserve]
        public HideFieldAttribute()
        {
        }

        public HideFieldAttribute(string fieldName)
        {
            this.fieldName = fieldName;
        }

        public HideFieldAttribute(string fieldName, object fieldValue)
        {
            this.fieldName = fieldName;
            this.fieldValue = fieldValue;
            useFieldValue = true;
        }
    }
}