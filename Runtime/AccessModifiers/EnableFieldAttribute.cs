﻿using System;

namespace Module.Inspector
{
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public sealed class EnableFieldAttribute : AccessModifierPropertyAttribute
    {
        public readonly string fieldName;
        public readonly bool useFieldValue;
        public readonly object fieldValue;

        public EnableFieldAttribute(string fieldName)
        {
            this.fieldName = fieldName;
        }

        public EnableFieldAttribute(string fieldName, object fieldValue)
        {
            this.fieldName = fieldName;
            this.fieldValue = fieldValue;
            useFieldValue = true;
        }
    }
}