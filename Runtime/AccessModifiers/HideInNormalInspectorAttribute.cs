using System;
using UnityEngine.Scripting;

namespace Module.Inspector
{
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = true, Inherited = true)]
    public sealed class HideInNormalInspectorAttribute : AccessModifierPropertyAttribute
    {
        [Preserve]
        public HideInNormalInspectorAttribute()
        {
        }
    }
}