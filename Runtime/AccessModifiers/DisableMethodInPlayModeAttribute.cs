﻿using System;
using UnityEngine.Scripting;

namespace Module.Inspector
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public sealed class DisableMethodInPlayModeAttribute : AccessModifierMethodAttribute
    {
        [Preserve]
        public DisableMethodInPlayModeAttribute()
        {
        }
    }
}