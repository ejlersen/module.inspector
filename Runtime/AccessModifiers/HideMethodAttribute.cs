﻿using System;
using UnityEngine.Scripting;

namespace Module.Inspector
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public sealed class HideMethodAttribute : AccessModifierMethodAttribute
    {
        public readonly string fieldName;
        public readonly bool useFieldValue;
        public readonly object fieldValue;

        [Preserve]
        public HideMethodAttribute()
        {
        }

        public HideMethodAttribute(string fieldName)
        {
            this.fieldName = fieldName;
        }

        public HideMethodAttribute(string fieldName, object fieldValue)
        {
            this.fieldName = fieldName;
            this.fieldValue = fieldValue;
            useFieldValue = true;
        }
    }
}