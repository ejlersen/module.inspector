﻿using System;

namespace Module.Inspector
{
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public sealed class DisableFieldAttribute : AccessModifierPropertyAttribute
    {
        public readonly string fieldName;
        public readonly bool useFieldValue;
        public readonly object fieldValue;

        public DisableFieldAttribute(string fieldName)
        {
            this.fieldName = fieldName;
        }

        public DisableFieldAttribute(string fieldName, object fieldValue)
        {
            this.fieldName = fieldName;
            this.fieldValue = fieldValue;
            useFieldValue = true;
        }
    }
}