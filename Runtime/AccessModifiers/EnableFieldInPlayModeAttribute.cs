﻿using System;
using UnityEngine.Scripting;

namespace Module.Inspector
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public sealed class EnableFieldInPlayModeAttribute : AccessModifierPropertyAttribute
    {
        [Preserve]
        public EnableFieldInPlayModeAttribute()
        {
        }
    }
}