﻿using System;
using UnityEngine.Scripting;

namespace Module.Inspector
{
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public sealed class DisableAttribute : AccessModifierPropertyAttribute
    {
        [Preserve]
        public DisableAttribute()
        {
        }
    }
}