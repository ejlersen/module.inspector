﻿using System;

namespace Module.Inspector
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public sealed class DisableMethodAttribute : AccessModifierMethodAttribute
    {
        public readonly string fieldName;
        public readonly bool useFieldValue;
        public readonly object fieldValue;

        public DisableMethodAttribute(string fieldName)
        {
            this.fieldName = fieldName;
        }

        public DisableMethodAttribute(string fieldName, object fieldValue)
        {
            this.fieldName = fieldName;
            this.fieldValue = fieldValue;
            useFieldValue = true;
        }
    }
}