using System;
using UnityEngine.Scripting;

namespace Module.Inspector
{
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public sealed class PopupFromScriptableObjectTypeAttribute : DrawerPropertyAttribute
    {
        public readonly Type type;

        [Preserve]
        public PopupFromScriptableObjectTypeAttribute()
        {
        }
        
        public PopupFromScriptableObjectTypeAttribute(Type type)
        {
            this.type = type;
        }
    }
}
