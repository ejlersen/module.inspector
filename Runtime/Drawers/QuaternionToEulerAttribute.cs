﻿using System;
using UnityEngine.Scripting;

namespace Module.Inspector
{
    #if UNITY_2021_1_OR_NEWER
    [Obsolete]
    #endif
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public sealed class QuaternionToEulerAttribute : DrawerPropertyAttribute
    {
        [Preserve]
        public QuaternionToEulerAttribute()
        {
        }
    }
}