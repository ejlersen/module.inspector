using System;

namespace Module.Inspector
{
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public sealed class StringToAnimatorParameterAttribute : DrawerPropertyAttribute
    {
        public readonly string animatorFieldName;

        public StringToAnimatorParameterAttribute(string animatorFieldName)
        {
            this.animatorFieldName = animatorFieldName;
        }
    }
}