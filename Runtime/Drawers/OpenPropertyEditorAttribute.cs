﻿#if UNITY_2019_3_OR_NEWER
using System;
using UnityEngine.Scripting;

namespace Module.Inspector
{
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public sealed class OpenPropertyEditorAttribute : DrawerPropertyAttribute
    {
        [Preserve]
        public OpenPropertyEditorAttribute()
        {
        }
    }
}
#endif