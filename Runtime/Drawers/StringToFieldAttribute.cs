﻿using System;

namespace Module.Inspector
{
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public sealed class StringToFieldAttribute : DrawerPropertyAttribute
    {
        public readonly Type type;

        public StringToFieldAttribute(Type type)
        {
            this.type = type;
        }
    }
}