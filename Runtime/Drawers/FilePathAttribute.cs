﻿using System;

namespace Module.Inspector
{
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public sealed class FilePathAttribute : DrawerPropertyAttribute
    {
        public readonly EFilePathType pathType;
        public readonly string extension = "*";

        public FilePathAttribute(EFilePathType pathType = EFilePathType.RelativeToProjectFolder)
        {
            this.pathType = pathType;
        }

        public FilePathAttribute(string extension, EFilePathType pathType = EFilePathType.RelativeToProjectFolder)
        {
            this.extension = extension;
            this.pathType = pathType;
        }
    }
}