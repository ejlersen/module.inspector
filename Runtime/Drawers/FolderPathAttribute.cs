﻿using System;

namespace Module.Inspector
{
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public sealed class FolderPathAttribute : DrawerPropertyAttribute
    {
        public readonly bool useAbsolute;

        public FolderPathAttribute(bool useAbsolute = false)
        {
            this.useAbsolute = useAbsolute;
        }
    }
}