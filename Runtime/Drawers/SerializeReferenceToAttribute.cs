﻿#if UNITY_2019_3_OR_NEWER
using System;

namespace Module.Inspector
{
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public sealed class SerializeReferenceToAttribute : DrawerPropertyAttribute
    {
        public readonly bool useType;
        public readonly Type type;
        public readonly bool useFullname;

        public SerializeReferenceToAttribute(bool useFullname = true)
        {
            useType = false;
            this.useFullname = useFullname;
        }
        
        public SerializeReferenceToAttribute(Type type, bool useFullname = true)
        {
            this.useFullname = useFullname;
            this.type = type;
            useType = true;
        }
    }
}
#endif