using System;

namespace Module.Inspector
{
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public sealed class IntToAnimatorParameterAttribute : DrawerPropertyAttribute
    {
        public readonly string animatorFieldName;

        public IntToAnimatorParameterAttribute(string animatorFieldName)
        {
            this.animatorFieldName = animatorFieldName;
        }
    }
}