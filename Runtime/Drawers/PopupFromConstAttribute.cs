﻿using System;

namespace Module.Inspector
{
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public sealed class PopupFromConstAttribute : DrawerPropertyAttribute
    {
        public readonly Type type;

        public PopupFromConstAttribute(Type type)
        {
            this.type = type;
        }
    }
}