﻿#if UNITY_2019_3_OR_NEWER
using System;

namespace Module.Inspector
{
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public sealed class ReadableScriptableObjectAttribute : DrawerPropertyAttribute
    {
        public readonly bool editable;

        public ReadableScriptableObjectAttribute()
        {
            editable = true;
        }
        
        public ReadableScriptableObjectAttribute(bool editable)
        {
            this.editable = editable;
        }
    }
}
#endif