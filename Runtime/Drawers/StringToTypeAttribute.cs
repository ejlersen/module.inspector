﻿using System;

namespace Module.Inspector
{
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public sealed class StringToTypeAttribute : DrawerPropertyAttribute
    {
        public readonly Type assignableFrom;

        public StringToTypeAttribute(Type assignableFrom)
        {
            this.assignableFrom = assignableFrom;
        }
    }
}