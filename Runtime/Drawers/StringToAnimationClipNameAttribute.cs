using System;

namespace Module.Inspector
{
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public sealed class StringToAnimationClipAttribute : DrawerPropertyAttribute
    {
        public readonly string animationFieldName;

        public StringToAnimationClipAttribute(string animationFieldName)
        {
            this.animationFieldName = animationFieldName;
        }
    }
}