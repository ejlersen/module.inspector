﻿using System;

namespace Module.Inspector
{
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public sealed class NamingAttribute : DrawerPropertyAttribute
    {
        public enum EPatternType : byte
        {
            /// <summary>
            /// Camel casing: thisIsCamelCasing
            /// </summary>
            CamelCasing,
            /// <summary>
            /// Pascal casing: ThisIsPascalCasing
            /// </summary>
            PascalCasing,
            /// <summary>
            /// Snake casing: this_is_snake_casing
            /// </summary>
            SnakeCasing,
            /// <summary>
            /// Snake casing: THIS_IS_SNAKE_CASING_WITH_ALL_CAPS
            /// </summary>
            SnakeCasingAllCaps,
            /// <summary>
            /// Kebab casing: this-is-kebab-casing
            /// </summary>
            KebabCasing,
            /// <summary>
            /// Kebab casing: THIS_IS_KEBAB_CASING_WITH_ALL_CAPS
            /// </summary>
            KebabCasingAllCaps
        }
        
        public readonly EPatternType type;

        public NamingAttribute(EPatternType type)
        {
            this.type = type;
        }
    }
}