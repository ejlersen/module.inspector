﻿using System;
using Module.Inspector.Utilities;
using UnityEngine.Scripting;

namespace Module.Inspector
{
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public sealed class IntToEnumAttribute : DrawerPropertyAttribute
    {
        private Type type;

        private readonly string strTypeReplaceWith;
        private bool hasTriedFetchReplaceType;

        [Preserve]
        public IntToEnumAttribute(Type type)
        {
            this.type = type;
            hasTriedFetchReplaceType = true;
        }

        [Preserve]
        public IntToEnumAttribute(Type type, string strTypeReplaceWith)
        {
            this.type = type;
            this.strTypeReplaceWith = strTypeReplaceWith;
        }

        public Type GetValidType()
        {
            if (hasTriedFetchReplaceType)
                return type;
            
            #if UNITY_EDITOR
            Type replaceType = ReflectionUtility.GetAnyTypeWithName(strTypeReplaceWith);
            #else
            Type replaceType = null;
            #endif
            
            hasTriedFetchReplaceType = true;

            if (replaceType != null)
                type = replaceType;

            return type;
        }
    }
}