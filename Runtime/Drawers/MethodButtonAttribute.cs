﻿using System;
using UnityEngine.Scripting;

namespace Module.Inspector
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public sealed class MethodButtonAttribute: DrawerMethodAttribute
    {
        public readonly string name;

        [Preserve]
        public MethodButtonAttribute()
        {
        }

        public MethodButtonAttribute(string name)
        {
            this.name = name;
        }
    }
}