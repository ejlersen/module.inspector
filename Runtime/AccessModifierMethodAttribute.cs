﻿namespace Module.Inspector
{
    public abstract class AccessModifierMethodAttribute : AbstractMethodAttribute
    {
        public override EMethodType Type => EMethodType.AccessModifier;
    }
}