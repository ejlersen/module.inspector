﻿using UnityEngine;

namespace Module.Inspector
{
    public abstract class AbstractPropertyAttribute : PropertyAttribute
    {
        public abstract EPropertyType Type { get; }
    }
}