﻿namespace Module.Inspector
{
    public abstract class PredrawerModifierPropertyAttribute : AbstractPropertyAttribute
    {
        public override EPropertyType Type => EPropertyType.PredrawerModifier;
    }
}