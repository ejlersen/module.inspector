﻿namespace Module.Inspector
{
    public abstract class ValueModifierPropertyAttribute : AbstractPropertyAttribute
    {
        public override EPropertyType Type => EPropertyType.ValueModifier;
    }
}