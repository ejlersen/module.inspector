﻿using System;

namespace Module.Inspector
{
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public sealed class FieldLabelFromTypeAttribute : PredrawerModifierPropertyAttribute
    {
    }
}