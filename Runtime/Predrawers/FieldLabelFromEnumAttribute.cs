﻿using System;

namespace Module.Inspector
{
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public sealed class FieldLabelFromEnumAttribute : PredrawerModifierPropertyAttribute
    {
        public readonly Type type;
        public readonly bool nicify;
        
        public FieldLabelFromEnumAttribute(Type type)
        {
            this.type = type;
            nicify = true;
        }

        public FieldLabelFromEnumAttribute(Type type, bool nicify)
        {
            this.type = type;
            this.nicify = nicify;
        }
    }
}