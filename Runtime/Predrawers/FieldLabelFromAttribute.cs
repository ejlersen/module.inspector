﻿using System;

namespace Module.Inspector
{
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public sealed class FieldLabelFromAttribute : PredrawerModifierPropertyAttribute
    {
        public readonly string fieldName;
        public readonly bool nicify;
        
        public FieldLabelFromAttribute(string fieldName)
        {
            this.fieldName = fieldName;
            nicify = true;
        }

        public FieldLabelFromAttribute(string fieldName, bool nicify)
        {
            this.fieldName = fieldName;
            this.nicify = nicify;
        }
    }
}