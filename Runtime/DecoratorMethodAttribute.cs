﻿namespace Module.Inspector
{
    public abstract class DecoratorMethodAttribute : AbstractMethodAttribute
    {
        public override EMethodType Type => EMethodType.Decorator;
    }
}