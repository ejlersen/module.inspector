﻿using UnityEngine;

namespace Module.Inspector
{
    public abstract class AbstractMethodAttribute : PropertyAttribute
    {
        public abstract EMethodType Type { get; }
    }
}