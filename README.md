# Description

This module contains a large collection of attributes that can be added either to a field or method. 
These attributes have different categories and some can be combined to create the desired behaviour for the field or method.


# Base

There are six different kinds of attributes:

* Access
    * Modify access to drawer or value change (E.g. Hide/Show, Disable/Enable input)
* Pre-drawer
    * Modify `SerializedProperty` or `GUIContent` label before drawing begins
* Drawer
    * Custom drawing of the field/method
* Value
    * Modify value before setting final result (E.g. clamp value)
* Validate
    * Validate field (if invalid, draws as red)
* Decorator
    * Adds additional information or styling to a field/method (E.g. Header displays a title/category to fields after attribute)
    


## Access

List of all available access attributes:

* `Disable`
    * Disables input to field
* `DisableField`
    * Disables input to field given value of another field
* `DisableFieldInPlayMode`
    * Disables input to field, when in play mode
* `DisableMethod`
    * Disables input to method (button) given value of another field
* `DisableMethodInPlayMode`
    * Disables input to method (button), when in play mode
* `EnableField`
    * Enable input to field given value of another field
* `EnableFieldInPlayMode`
    * Enable input to field, when in play mode
* `EnableMethod`
    * Enable input to method (button) given value of another field
* `EnableMethodInPlayMode`
    * Enable input to method (button), when in play mode
* `HideField`
    * Hide field given value of another field
* `HideInNormalInspector`
    * Hide field, if inspector is set to normal mode
* `HideMethod`
    * Hide method (button) given value of another field
* `ShowField`
    * Show field given value of another field
* `ShowMethod`
    * Show method (button) given value of another field



## Pre-drawer

List of all available pre-drawer attributes:

* `FieldLabelFrom`
    * Sets GUIContent label for `SerializedProperty` to value from another field
* `FieldLabelFromEnum`
    * Sets GUIContent label for `SerializedProperty` to enum name value of array index in parent field
* `FieldLabelFromType`
    * Sets GUIContent label for `SerializedProperty` to value type from another field
* `FieldLabelFromToString`
  * Sets GUIContent label for `SerializedProperty` to values ToString-method
* `HideLabel`
    * Sets GUIContent label for `SerializedProperty` to empty string 


## Drawer

List of all available drawer attributes:

* `EnumFlag`
    * Adds popup with enum type provided and allows for selecting values as flags (Requirement: Enum must have flag values)
* `FilePath`
    * Adds file window and selected file is converted to string path
* `FolderPath`
    * Adds folder window and selected folder is converted to string path
* `IntToAnimatorParameter`
    * Adds popup with all animator parameter names provided by animator field and converts to hash id
* `IntToEnum`
    * Adds a popup with enum type provided and converts it to an integer
* `IntToLayer`
    * Adds a popup with a single layer selection and converts it to an integer
* `IntToLayerMask`
    * Adds a popup with a multiple layer selection and converts it to an integer
* `MethodButton`
    * Adds a button in the inspector that invokes the method (Requirement: No arguments)
* `Naming`
    * Adds button to apply naming scheme to string value
    * Types: Camel, Pascal, Snake, Snake (All caps), Kebab, Kebab (All caps)
* `OpenPropertyEditor`
    * Adds a button to open a property window, if object reference is not null and either a `ScriptableObject` or `Component`
    * _Note: `OpenPropertyEditor` and `ReadableScriptableObject` currently doesn't support each other_
* `Percentage`
    * Convert float value to percentage and back again (1% = 0.01f)
* `PopupFromConst`
    * Adds popup with all const fields of type string in provided type
* `PopupFromScriptableObjectType`
    * Adds popup with all `ScriptableObject`s from field or provided type
* `QuaternionToEuler`
    * Converts quaternion value to Euler angles and back again
* `ReadableScriptableObject`
    * Creates an inline editor in the inspector window, so you can edit `ScriptableObjects` without finding the object first
* `SceneDropdown`
    * Adds a popup with all scenes in EditorBuildSettings scenes
* `SerializeReferenceTo`
    * Adds a popup for `SerializeReference` fields with types inheriting from assigned type or field type
* `SingleSelectionFlag`
    * Adds popup, where only a single value from the flag can be selected
* `Slider`
    * Adds a min & max slider and clamps value (Requirement: `MinValue` and `MaxValue`)
* `StringToAnimatorParameter`
    * Adds popup with all animator parameter names provided by animator field specified
* `StringToField`
    * Adds button to open window to find all fields on object of provided type and sets string with selected field name
* `StringToType`
    * Adds button to open window to find all types assignable from provided type and sets string with selected type name
* `Tag`
    * Adds popup with all tag values for field of type string
* `UrlGoTo`
    * Adds a button to the field that calls `Application.OpenUrl` with string value

  

## Value

List of all value attributes:

* `ArrayIndex`
    * Clamps value between other fields array size `[0;size[`
* `AssetsOnly`
    * If value is not an asset it will be set to null
* `AssignAssetIfNull`
    * If value is null will assign first asset found using AssetDatabase.FindAssets
* `AssignComponentIfNull`
    * If value is null will either use field or custom type to get from self or children
* `LargerThanField`
    * If value is greater than other fields value, then set value to other fields value
* `MaxValue`
    * If value is greater than max value, then set value to max value
* `MinValue`
    * If value is less than min value, then set value to min value
* `SceneObjectsOnly`
    * If value is an asset it will be set to null
* `SmallerThanField`
    * If value is less than other fields value, then set value to other fields value



## Validate

List of all validate attributes:

* `NotNullField`
    * Checks if field is null



## Decorators

List of all decorator attributes:

* `HorizontalLine`
    * Draws a horizontal line above the field
* `MethodHeader`
    * Draws a header/title/category text above the method (button)



## Show Hidden Fields

To allow fields hidden from inspector either by `NonSerialized` or `HideInInspector` add the following to the class:

* `EnableShowHiddenFields`
    * Enables an additional search to find any "hidden" fields and draw them below `DefaultInspector`
* `ShowHiddenField`
    * Optional attribute for fields, if `EnableShowHiddenFields` has parameter `true`	



## Handles

List of all handle attributes:

* `BoxHandle`
    * Resize a box shaped value, like Vector3
	* Optional: Provide a `fieldPosition` and/or `fieldRotation` for handles to position and rotate
* `CircleHandle`
    * Resize a circle shaped value, like float
	* Optional: Provide a `fieldPosition` for handle to position
* `LabelHandle`
    * Write text or field values
	* Optional: Provide a `fieldPosition` and/or `fieldRotation` for handles to position and rotate
* `PositionHandle`
    * Position handle with option to set number of axis visible (X, Y, Z)
* `RectangleHandle`
    * Resize a rectangle shaped value, like Vector2
	* Optional: Provide a `fieldPosition` and/or `fieldRotation` for handles to position and rotate
* `RotationHandle`
    * Rotation handle for world or self-spaced rotation
	* Optional: Provide a `fieldPosition` for handle to position
* `SphereHandle`
    * Resize a sphere shaped value, like float
	* Optional: Provide a `fieldPosition` and/or `fieldRotation` for handles to position and rotate
